package com.app.amigo.mathtravel.gameProgress;

import android.os.Bundle;
import android.view.View;

public class NormalGame extends GameProgressFragment {

    private static int counterNormalGameFragment;

    public static int getCounterNormalGameFragment() {
        return counterNormalGameFragment;
    }

    public static void setCounterNormalGameFragment(int counterNormalGameFragment) {
        NormalGame.counterNormalGameFragment = counterNormalGameFragment;
    }

    @Override
    int counterFragment() {
        return counterNormalGameFragment;
    }

    @Override
    long timeOut() {
        return 0;
    }

    @Override
    void createView(View view) {
        createProgressBar(true,view);
        createTimeProgressBar(false,view);
        createRecordLayout(false,view);
    }

    public static NormalGame newInstance() {
        counterNormalGameFragment++;
        Bundle args = new Bundle();
        NormalGame fragment = new NormalGame();
        fragment.setArguments(args);
        return fragment;
    }
}
