package com.app.amigo.mathtravel.buildFragment;

import android.os.Bundle;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.planets.ThirdPlanet;

public class ThirdSpaceship extends  BuildFragment {

    private static ThirdSpaceship thirdSpaceship;

    @Override
    BuildFragment buildFragment() {
        return thirdSpaceship;
    }

    @Override
    public String planet() {
        return MyDatabase.THREE_PLANET;
    }

    @Override
    int backgroundPlanet() {
        return R.drawable.fon_build_three;
    }

    @Override
    int backgroundScheme() {
        return R.drawable.rocket_three_stars_and_scheme;
    }

    @Override
    public int countParts() {
        return 5;
    }

    @Override
    int rocketPart1() {
        return R.drawable.rocket_three_part1;
    }

    @Override
    int rocketPart2() {
        return R.drawable.rocket_three_part2;
    }

    @Override
    int rocketPart3() {
        return R.drawable.rocket_three_part3;
    }

    @Override
    int rocketPart4() {
        return R.drawable.rocket_three_part4;
    }

    @Override
    int rocketPart5() {
        return R.drawable.rocket_three_part5;
    }

    @Override
    int rocketPart6() {
        return 0;
    }

    @Override
    int rocketPart7() {
        return 0;
    }

    @Override
    void createView(View view) {
        txtPart1.setVisibility(View.VISIBLE);
        txtPart2.setVisibility(View.VISIBLE);
        txtPart3.setVisibility(View.VISIBLE);
        txtPart4.setVisibility(View.VISIBLE);
        txtPart5.setVisibility(View.VISIBLE);

        txtPart1.setText("10");
        txtPart2.setText("20");
        txtPart3.setText("30");
        txtPart4.setText("40");
        txtPart5.setText("50");
    }

    @Override
    protected void setConstraintParam() {
        constraintSet.setVerticalBias(txtPart1.getId(), 0.70f);
        constraintSet.setHorizontalBias(txtPart1.getId(), 0.512f);
        constraintSet.setVerticalBias(txtPart2.getId(), 0.44f);
        constraintSet.setHorizontalBias(txtPart2.getId(), 0.512f);
        constraintSet.setVerticalBias(txtPart3.getId(), 0.250f);
        constraintSet.setHorizontalBias(txtPart3.getId(), 0.512f);
        constraintSet.setVerticalBias(txtPart4.getId(), 0.780f);
        constraintSet.setHorizontalBias(txtPart4.getId(), 0.332f);
        constraintSet.setVerticalBias(txtPart5.getId(), 0.780f);
        constraintSet.setHorizontalBias(txtPart5.getId(), 0.692f);
    }

    public static ThirdSpaceship getThirdSpaceship() {
        if(thirdSpaceship == null){
            thirdSpaceship = new ThirdSpaceship();
            return thirdSpaceship;
        }else return thirdSpaceship;
    }
}
