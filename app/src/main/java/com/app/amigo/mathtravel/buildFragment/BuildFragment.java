package com.app.amigo.mathtravel.buildFragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.amigo.mathtravel.PlayPagerActivity;
import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.database.MyDbHelper;
import com.app.amigo.mathtravel.game.OnBackPressedListener;
import com.app.amigo.mathtravel.game.RocketBuiltFragment;
import com.app.amigo.mathtravel.planets.FifthPlanet;
import com.app.amigo.mathtravel.planets.FirstPlanet;
import com.app.amigo.mathtravel.planets.FourthPlanet;
import com.app.amigo.mathtravel.planets.SecondPlanet;
import com.app.amigo.mathtravel.planets.ThirdPlanet;

public abstract class BuildFragment extends Fragment implements OnBackPressedListener, View.OnClickListener {

    public static final String TAG = "BuildFragment";
    protected MyDbHelper myDbHelper;

    private ImageView fonBuildFragment;
    private ImageView scheme;
    private ImageView part1;
    private ImageView part2;
    private ImageView part3;
    private ImageView part4;
    private ImageView part5;
    private ImageView part6;
    private ImageView part7;

    private int currentGold;

    protected TextView txtPart1;
    protected TextView txtPart2;
    protected TextView txtPart3;
    protected TextView txtPart4;
    protected TextView txtPart5;
    protected TextView txtPart6;
    protected TextView txtPart7;

    protected ConstraintLayout constraintLayout;
    protected ConstraintSet constraintSet;

    public abstract String planet();
    abstract BuildFragment buildFragment();
    abstract int backgroundPlanet();
    abstract int backgroundScheme();
    public abstract int countParts();
    abstract int rocketPart1();
    abstract int rocketPart2();
    abstract int rocketPart3();
    abstract int rocketPart4();
    abstract int rocketPart5();
    abstract int rocketPart6();
    abstract int rocketPart7();
    abstract void createView(View view);
    protected abstract void setConstraintParam();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.build_fragment,container,false);
        myDbHelper = MyDbHelper.getInstance(getContext());
//        currentGold = Integer.parseInt(myDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));

        initView(view);
        createView(view);
        Log.i(TAG,"onCreateView");

        return view;
    }

    private void initView(View view){

        fonBuildFragment = view.findViewById(R.id.fon_build_fragment);
        fonBuildFragment.setImageResource(backgroundPlanet());
        fonBuildFragment.setScaleType(ImageView.ScaleType.FIT_XY);
        scheme = view.findViewById(R.id.rocket_scheme);
        scheme.setImageResource(backgroundScheme());

        part1 = view.findViewById(R.id.rocket_p1);
        part2 = view.findViewById(R.id.rocket_p2);
        part3 = view.findViewById(R.id.rocket_p3);
        part4 = view.findViewById(R.id.rocket_p4);
        part5 = view.findViewById(R.id.rocket_p5);
        part6 = view.findViewById(R.id.rocket_p6);
        part7 = view.findViewById(R.id.rocket_p7);

        part1.setImageResource(rocketPart1());
        part2.setImageResource(rocketPart2());
        part3.setImageResource(rocketPart3());
        part4.setImageResource(rocketPart4());
        part5.setImageResource(rocketPart5());
        part6.setImageResource(rocketPart6());
        part7.setImageResource(rocketPart7());

        txtPart1 = view.findViewById(R.id.txtP1);
        txtPart2 = view.findViewById(R.id.txtP2);
        txtPart3 = view.findViewById(R.id.txtP3);
        txtPart4 = view.findViewById(R.id.txtP4);
        txtPart5 = view.findViewById(R.id.txtP5);
        txtPart6 = view.findViewById(R.id.txtP6);
        txtPart7 = view.findViewById(R.id.txtP7);

        constraintLayout = view.findViewById(R.id.constraintLayout);
        constraintSet =  new ConstraintSet();
        constraintSet.clone(constraintLayout);
        setConstraintParam();
        constraintSet.applyTo(constraintLayout);

        txtPart1.setOnClickListener(this);
        txtPart2.setOnClickListener(this);
        txtPart3.setOnClickListener(this);
        txtPart4.setOnClickListener(this);
        txtPart5.setOnClickListener(this);
        txtPart6.setOnClickListener(this);
        txtPart7.setOnClickListener(this);

        if(planet().equals(planet())) {
            int isP1 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P1);
            if (isP1 == 1) part1.setVisibility(View.VISIBLE);

            int isP2 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P2);
            if (isP2 == 1) part2.setVisibility(View.VISIBLE);

            int isP3 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P3);
            if (isP3 == 1) part3.setVisibility(View.VISIBLE);

            int isP4 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P4);
            if (isP4 == 1) part4.setVisibility(View.VISIBLE);

            int isP5 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P5);
            if (isP5 == 1) part5.setVisibility(View.VISIBLE);

            int isP6 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P6);
            if (isP6 == 1) part6.setVisibility(View.VISIBLE);

            int isP7 = myDbHelper.getIsBuyPart(planet(), MyDatabase.PART_P7);
            if (isP7 == 1) part7.setVisibility(View.VISIBLE);
        }
    }

    private void buyPart(ImageView view, String partInDatabase, TextView txt ){
        int isPart = myDbHelper.getIsBuyPart(planet(), partInDatabase);
        Log.i(TAG,"in base: " + isPart);
        if( isPart < 1) {
            int topPrice = Integer.parseInt(txt.getText().toString());
            currentGold = Integer.parseInt(myDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));
            if (currentGold >= topPrice) {
                myDbHelper.updateAward(MyDatabase.GOLD_COLUMN, (currentGold - topPrice));
                PlayPagerActivity.mCounterGold.setText(myDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));
                view.setVisibility(View.VISIBLE);
                myDbHelper.updateBuyPart(planet(), partInDatabase, 1);
                if(myDbHelper.allParts(buildFragment())){
                    Log.i(TAG,"rocket is BUILD");

                    if(!planet().equals(MyDatabase.FIVE_PLANET)) addFragmentIfBuyAllParts();

                    switch (planet()){
                        case (MyDatabase.ONE_PLANET) :
                                myDbHelper.updateIsActiveButton(MyDatabase.TWO_PLANET, MyDatabase.ONE_FIND_BUTTON, 1);
                                myDbHelper.updateIsActiveButton(MyDatabase.TWO_PLANET, MyDatabase.ONE_EXAMPLES_BUTTON, 1);
                                myDbHelper.updateIsActiveButton(MyDatabase.TWO_PLANET, MyDatabase.ONE_MORE_LESS_BUTTON, 1);
                            FirstPlanet.getFirstPlanet().setImageSpaceship();
                            SecondPlanet secondPlanet = SecondPlanet.getSecondPlanet();
                            secondPlanet.setIsActiveBtn();
                            break;

                        case (MyDatabase.TWO_PLANET) :
                              myDbHelper.updateIsActiveButton(MyDatabase.THREE_PLANET, MyDatabase.ONE_FIND_BUTTON, 1);
                              myDbHelper.updateIsActiveButton(MyDatabase.THREE_PLANET, MyDatabase.ONE_EXAMPLES_BUTTON, 1);
                              myDbHelper.updateIsActiveButton(MyDatabase.THREE_PLANET, MyDatabase.ONE_MORE_LESS_BUTTON, 1);
                            SecondPlanet.getSecondPlanet().setImageSpaceship();
                            ThirdPlanet thirdPlanet = ThirdPlanet.getThirdPlanet();
                            thirdPlanet.setIsActiveBtn();
                            break;

                        case (MyDatabase.THREE_PLANET) :
                            myDbHelper.updateIsActiveButton(MyDatabase.FOUR_PLANET, MyDatabase.ONE_FIND_BUTTON, 1);
                            myDbHelper.updateIsActiveButton(MyDatabase.FOUR_PLANET, MyDatabase.ONE_EXAMPLES_BUTTON, 1);
                            ThirdPlanet.getThirdPlanet().setImageSpaceship();
                            FourthPlanet fourthPlanet = FourthPlanet.getFourthPlanet();
                            fourthPlanet.setIsActiveBtn();
                            break;

                        case (MyDatabase.FOUR_PLANET) :
                            myDbHelper.updateIsActiveButton(MyDatabase.FIVE_PLANET, MyDatabase.ONE_FIND_BUTTON, 1);
                            myDbHelper.updateIsActiveButton(MyDatabase.FIVE_PLANET, MyDatabase.ONE_EXAMPLES_BUTTON, 1);
                            FourthPlanet.getFourthPlanet().setImageSpaceship();
                            FifthPlanet fifthPlanet = FifthPlanet.getFifthPlanet();
                            fifthPlanet.setIsActiveBtn();
                            break;

                        case (MyDatabase.FIVE_PLANET) :
                            FifthPlanet.getFifthPlanet().setImageSpaceship();
                            break;

                            default:break;
                    }

                }
            }
        } else {
            if(!planet().equals(MyDatabase.FIVE_PLANET)) {
                if (myDbHelper.allParts(buildFragment())) addFragmentIfBuyAllParts();
            }else {
                if (myDbHelper.allParts(buildFragment())) {
                    RocketBuiltFragment.setFivePlanet(true);
                    addFragmentIfBuyAllParts();
                }
            }
        }
    }

    public void addFragmentIfBuyAllParts(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(!isFragment("RocketBuiltFragment") && !isFragment("AnswerFragment") ) {
                    addAnswerFragment(RocketBuiltFragment.newInstance(), "RocketBuiltFragment");
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(runnable,200);
    }

    public void addAnswerFragment(Fragment fragment, String tag){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.gameAnswerContainer, fragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public boolean isFragment(String tag){
        if (getFragmentManager() != null) {
            Fragment fragment = getFragmentManager().findFragmentByTag(tag);
            if (fragment != null && fragment.isVisible()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){
            case (R.id.txtP1) :
                buyPart(part1, MyDatabase.PART_P1, txtPart1 );
                break;

            case (R.id.txtP2) :
                buyPart(part2, MyDatabase.PART_P2, txtPart2 );
                break;

            case (R.id.txtP3) :
                buyPart(part3, MyDatabase.PART_P3, txtPart3 );
                break;

            case (R.id.txtP4) :
                buyPart(part4, MyDatabase.PART_P4, txtPart4 );
                break;

            case (R.id.txtP5) :
                buyPart(part5, MyDatabase.PART_P5, txtPart5 );
                break;

            case (R.id.txtP6) :
                buyPart(part6, MyDatabase.PART_P6, txtPart6 );
                break;

            case (R.id.txtP7) :
                buyPart(part7, MyDatabase.PART_P7, txtPart7 );
                break;


        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG,"onDestroyView");
    }

    @Override
    public void onBackPressed() {
        if(!isFragment("RocketBuiltFragment"))
        getFragmentManager().popBackStack();
    }
}

