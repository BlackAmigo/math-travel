package com.app.amigo.mathtravel.gameProgress;

import android.os.Bundle;
import android.view.View;

public class QuickGame extends GameProgressFragment {

    private static int counterQuickGameFragment;
    private static QuickGame quickGameFragment;

    public static int getCounterQuickGameFragment() {
        return counterQuickGameFragment;
    }

    public static void setCounterQuickGameFragment(int counterQuickGameFragment) {
        QuickGame.counterQuickGameFragment = counterQuickGameFragment;
    }

    @Override
    int counterFragment() {
        return counterQuickGameFragment;
    }

    @Override
    long timeOut() {
        return 3000;
    }

    @Override
    void createView(View view) {
        createProgressBar(true,view);
        createTimeProgressBar(true,view);
        createRecordLayout(false,view);
    }

    public static QuickGame newInstance() {
        counterQuickGameFragment++;
        Bundle args = new Bundle();
        quickGameFragment = new QuickGame();
        quickGameFragment.setArguments(args);
        return quickGameFragment;
    }

    public  static QuickGame getFragment (){
        if(quickGameFragment != null)return quickGameFragment;
        else return null;
    }

    public void startMyTotalTask (long delayMillis){
        startMyBarTask(delayMillis);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        quickGameFragment = null;
    }
}
