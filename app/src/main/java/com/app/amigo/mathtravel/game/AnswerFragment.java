package com.app.amigo.mathtravel.game;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.gameProgress.NormalGame;
import com.app.amigo.mathtravel.gameProgress.QuickGame;
import com.app.amigo.mathtravel.gameProgress.RecordGame;
import com.app.amigo.mathtravel.gameProgress.TotalGame;

public class AnswerFragment extends HelperTypes implements View.OnClickListener, OnBackPressedListener{

    private final static String TAG = "AnswerFragment";

    public static boolean isBackPressed = false;
    public static boolean isTimeOut = false;
    public static boolean isWin = false;

    public static AnswerFragment newInstance() {

        Bundle args = new Bundle();
        AnswerFragment fragment = new AnswerFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_answer,container,false);

        TextView txtInfo = view.findViewById(R.id.text_info);
        TextView txtInfo2 = view.findViewById(R.id.text_info2);
//        ImageView gold = view.findViewById(R.id.icon_gold_in_answer);
//        TextView txtMark = view.findViewById(R.id.txt_exclamation_mark);
//        TextView txtReward = view.findViewById(R.id.txt_reward);
        if(!isWin) {
//            gold.setVisibility(View.GONE);
//            txtMark.setVisibility(View.GONE);
//            txtReward.setVisibility(View.GONE);

            if (isBackPressed) {
                txtInfo.setText("Хотите продолжить?");
            } else {
                if (!isTimeOut) {
                    txtInfo.setText("Не правильно!");
                    txtInfo2.setText("Продолжить заново ?");
                } else {
                    txtInfo.setText("Время вышло...!");
                    txtInfo2.setText("Продолжить заново ?");
                }
            }
        }else {
            updateGold();
            txtInfo.setText("Все Верно!");
            txtInfo2.setText("Следующий Уровень Открыт!");
//            txtMark.setText("!");
//            txtReward.setText("100");
        }
        initButtons(view);

        return view;
    }

    private void initButtons(View view){
        Button continueButton = view.findViewById(R.id.continueButton);
        continueButton.setText("Продолжить");
        Button backToMenuButton = view.findViewById(R.id.backToMenuButton);
        backToMenuButton.setText("Назад в Меню");
        if(isWin){
            backToMenuButton.setVisibility(View.GONE);
        }
        continueButton.setOnClickListener(this);
        backToMenuButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Log.i(TAG,"onClick");
    if(view.getId() == R.id.continueButton) {
        if (!isWin) {
            if (isBackPressed) {
                getFragmentManager().popBackStack();
                if (isFragment("TotalGame")) {
                    TotalGame.getFragment().startMyTotalTask(100);
                } else if (isFragment("QuickGame")) {
                    QuickGame.getFragment().startMyTotalTask(100);
                } else if (isFragment("RecordGame")) {
                    RecordGame.getFragment().startMyTotalTask(100);
                }
                isBackPressed = false;
            } else {
                if (isTimeOut) {
                    getFragmentManager().popBackStack();
                    isTimeOut = false;
                    restartFragment();
                } else {
                    getFragmentManager().popBackStack();
                    restartFragment();
                }
            }
        }else {
            getFragmentManager().popBackStack();
            setCounterProgressFragment(0);
            removeProgressAndTypeFragments();
            isTimeOut = false;
            isBackPressed = false;
            isWin = false;
            clearStack();
        }
    }

    if(view.getId() == R.id.backToMenuButton){
            getFragmentManager().popBackStack();
            setCounterProgressFragment(0);
            removeProgressAndTypeFragments();
            isTimeOut = false;
            isBackPressed = false;
            clearStack();
        }

    }
    private void setCounterProgressFragment(int count){
        if (isFragment("NormalGame")){
            NormalGame.setCounterNormalGameFragment(count);}

        else if(isFragment("TotalGame")){
            TotalGame.setCounterTotalGameFragment(count); }

        else if(isFragment("QuickGame")){
            QuickGame.setCounterQuickGameFragment(count); }

        else if(isFragment("RecordGame")){
            RecordGame.setCounterRecordGameFragment(count);}
    }

    private void restartFragment(){
        setCounterProgressFragment(0);
        replaceCurrentFragments();
    }

    @Override
    public void onBackPressed() {

    }

}
