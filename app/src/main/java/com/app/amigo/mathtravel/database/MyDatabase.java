package com.app.amigo.mathtravel.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MyDatabase extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "myDB";
    //    private final int [] goldOfPlanet = {1,10,100,1000,10000};
    public static final String INCOME_TABLE = "income_table";
    public static final String ID_COLUMN = "_id";
    public static final String GOLD_COLUMN = "gold";
    public static final String FUEL_COLUMN = "fuel";
    public static final String TOTAL_LINE = "total";

    public static final String ONE_PLANET = "one_planet";
    public static final String TWO_PLANET = "two_planet";
    public static final String THREE_PLANET = "three_planet";
    public static final String FOUR_PLANET = "four_planet";
    public static final String FIVE_PLANET = "five_planet";

    public static final String BUY_TABLE = "buy_table";
    public static final String PART_P1 = "part_p1";
    public static final String PART_P2 = "part_p2";
    public static final String PART_P3 = "part_p3";
    public static final String PART_P4 = "part_p4";
    public static final String PART_P5 = "part_p5";
    public static final String PART_P6 = "part_p6";
    public static final String PART_P7 = "part_p7";
    private static final String [] PARTS = {PART_P1,PART_P2,PART_P3,PART_P4,PART_P5,PART_P6,PART_P7};
    private static final String [] PLANETS = {ONE_PLANET,TWO_PLANET,THREE_PLANET,FOUR_PLANET,FIVE_PLANET};

    public static final String BUTTON_TABLE = "button_table";
    public static final String ONE_FIND_BUTTON = "mOneFindCountButton";
    public static final String TWO_FIND_BUTTON = "mTwoFindCountButton";
    public static final String THREE_FIND_BUTTON = "mThreeFindCountButton";
    public static final String FOUR_FIND_BUTTON = "mFourFindCountButton";
    public static final String ONE_EXAMPLES_BUTTON = "mOneExamplesButton";
    public static final String TWO_EXAMPLES_BUTTON = "mTwoExamplesButton";
    public static final String THREE_EXAMPLES_BUTTON = "mThreeExamplesButton";
    public static final String FOUR_EXAMPLES_BUTTON = "mFourExamplesButton";
    public static final String FIVE_EXAMPLES_BUTTON = "mFiveExamplesButton";
    public static final String SIX_EXAMPLES_BUTTON = "mSixExamplesButton";
    public static final String ONE_MORE_LESS_BUTTON = "mOneMoreLessButton";
    public static final String TWO_MORE_LESS_BUTTON = "mTwoMoreLessButton";
    public static final String THREE_MORE_LESS_BUTTON = "mThreeMoreLessButton";
    public static final String FOUR_MORE_LESS_BUTTON = "mFourMoreLessButton";
    public static final String [] BUTTONS = {ONE_FIND_BUTTON, TWO_FIND_BUTTON, THREE_FIND_BUTTON, FOUR_FIND_BUTTON, ONE_EXAMPLES_BUTTON, TWO_EXAMPLES_BUTTON, THREE_EXAMPLES_BUTTON,
            FOUR_EXAMPLES_BUTTON, FIVE_EXAMPLES_BUTTON, SIX_EXAMPLES_BUTTON, ONE_MORE_LESS_BUTTON, TWO_MORE_LESS_BUTTON, THREE_MORE_LESS_BUTTON, FOUR_MORE_LESS_BUTTON};

    public static String[] getParts() {
        return PARTS;
    }

    public static String[] getPlanets() {
        return PLANETS;
    }

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
//        Log.d(LOG_TAG, "--- onCreate database ---");
        ContentValues cv = new ContentValues();
        String createIncomeTable = "create table " + INCOME_TABLE + " " + "("+ ID_COLUMN + " primary key, " + GOLD_COLUMN + " integer," + FUEL_COLUMN + " integer" + ")";
        db.execSQL(createIncomeTable);

        cv.put(ID_COLUMN, TOTAL_LINE);
        cv.put(GOLD_COLUMN, 0);
        cv.put(FUEL_COLUMN, 0);
        db.insert(INCOME_TABLE,null,cv);

        ContentValues cv2 = new ContentValues();

        String createBuyTable = "create table " + BUY_TABLE + " " + "("+ ID_COLUMN + " primary key, " + PART_P1 + " integer," +
                PART_P2 + " integer," +  PART_P3 + " integer," + PART_P4 + " integer," +
                PART_P5 + " integer," + PART_P6 + " integer," +  PART_P7 + " integer" + ")";
        db.execSQL(createBuyTable);

        for (int i = 0; i < PLANETS.length; i++) {
            cv2.put(ID_COLUMN, PLANETS[i]);
            for (int j = 0; j < PARTS.length; j++) {
                cv2.put(PARTS[j],0);
            }
            db.insert(BUY_TABLE,null,cv2);
        }

        ContentValues cv3 = new ContentValues();

        String createButtonTable = "create table " + BUTTON_TABLE + " " + "("+ ID_COLUMN + " primary key, " + ONE_FIND_BUTTON + " integer," +
                TWO_FIND_BUTTON + " integer," +  THREE_FIND_BUTTON + " integer," + FOUR_FIND_BUTTON + " integer," +
                ONE_EXAMPLES_BUTTON + " integer," + TWO_EXAMPLES_BUTTON + " integer," + THREE_EXAMPLES_BUTTON + " integer," +
                FOUR_EXAMPLES_BUTTON + " integer," +   FIVE_EXAMPLES_BUTTON + " integer," +   SIX_EXAMPLES_BUTTON + " integer," +
                ONE_MORE_LESS_BUTTON + " integer," +   TWO_MORE_LESS_BUTTON + " integer," +   THREE_MORE_LESS_BUTTON + " integer," + FOUR_MORE_LESS_BUTTON + " integer" + ")";
        db.execSQL(createButtonTable);

        for (int i = 0; i < PLANETS.length; i++) {
            cv3.put(ID_COLUMN, PLANETS[i]);
            for (int j = 0; j < BUTTONS.length; j++) {
                cv3.put(BUTTONS[j],0);
            }
            db.insert(BUTTON_TABLE,null,cv3);
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
