package com.app.amigo.mathtravel.gameProgress;

import android.util.Log;

public class MyTask {

    public static GameProgressFragment.MyBarTask MT;

    public static GameProgressFragment.MyBarTask getMt() {
        return MT;
    }

    public static void setMt(GameProgressFragment.MyBarTask mt) {
        MT = mt;
    }

    public static void cancel(){
        if (MT != null) MT.cancel(true);
    }

    public static void execute(int i){
       try {
           if (MT != null) MT.execute(i);
       }catch (IllegalStateException e){
           Log.i("MyTask", "Cannot execute task: the task is already running.");
       }catch (Exception e){
           e.printStackTrace();
       }
    }
}
