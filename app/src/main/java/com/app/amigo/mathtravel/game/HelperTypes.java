package com.app.amigo.mathtravel.game;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.widget.TextView;

import com.app.amigo.mathtravel.PlayPagerActivity;
import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.database.MyDbHelper;
import com.app.amigo.mathtravel.gameProgress.MyTask;
import com.app.amigo.mathtravel.gameProgress.NormalGame;
import com.app.amigo.mathtravel.gameProgress.QuickGame;
import com.app.amigo.mathtravel.gameProgress.RecordGame;
import com.app.amigo.mathtravel.gameProgress.TotalGame;
import com.app.amigo.mathtravel.gameTypes.examples.CreateExample;
import com.app.amigo.mathtravel.gameTypes.examples.Examples;
import com.app.amigo.mathtravel.gameTypes.examples.ExamplesTest;
import com.app.amigo.mathtravel.gameTypes.findCount.FindCount;
import com.app.amigo.mathtravel.gameTypes.findCount.FindCountTest;
import com.app.amigo.mathtravel.gameTypes.moreLess.MoreLess;
import com.app.amigo.mathtravel.gameTypes.moreLess.MoreLessTest;
import com.app.amigo.mathtravel.planets.CountPlanet;
import com.app.amigo.mathtravel.planets.FirstPlanet;
import com.app.amigo.mathtravel.planets.FifthPlanet;
import com.app.amigo.mathtravel.planets.FourthPlanet;
import com.app.amigo.mathtravel.planets.Planet;
import com.app.amigo.mathtravel.planets.SecondPlanet;
import com.app.amigo.mathtravel.planets.ThirdPlanet;

public class HelperTypes extends  Fragment implements OnBackPressedListener {

    public static boolean clickable = false;

    private String redText = "#df0404";
    private String greenText = "#0fdf04";
    private FragmentTransaction transaction;
    public MyDbHelper mMyDbHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mMyDbHelper = MyDbHelper.getInstance(getContext());

    }

    public void updateGold (){
        int currentGold = Integer.parseInt(mMyDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));
        int newGold = currentGold + getCountPlanet();
        mMyDbHelper.updateAward(MyDatabase.GOLD_COLUMN, newGold);
        PlayPagerActivity.mCounterGold.setText(mMyDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));
    }

    public void updateFuel(){
        int currentFuel = Integer.parseInt(mMyDbHelper.getCurrentAward(MyDatabase.FUEL_COLUMN));
        int newFuel = currentFuel + getCountPlanet();
        mMyDbHelper.updateAward(MyDatabase.FUEL_COLUMN, newFuel);
        PlayPagerActivity.mCounterFuel.setText(mMyDbHelper.getCurrentAward(MyDatabase.FUEL_COLUMN));
    }

    public void clickableDelay(){
            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    clickable = true;
                }
            };
            Handler h = new Handler();
            h.postDelayed(runnable,500);
    }

    public void setTextColor(boolean isTrue, TextView... textViews){
        if(isTrue){
            for(TextView txt : textViews){
                txt.setTextColor(Color.parseColor(greenText));
            }
        }else {
            for(TextView txt : textViews){
                txt.setTextColor(Color.parseColor(redText));
            }
        }
    }

    public void replaceFragmentsIfRightAnswer(){

        MyTask.cancel();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(!isFragment("AnswerFragment")) {
                    replaceCurrentFragments();
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(runnable,500);
    }

    public void replaceCurrentFragments(){
        if (isFragment("NormalGame")){
            replaceFragments(NormalGame.newInstance(),"NormalGame",returnGameType());
        }
        else if(isFragment("TotalGame")){
            replaceFragments(TotalGame.newInstance(),"TotalGame",returnGameType());
        }
        else if(isFragment("QuickGame")){
            replaceFragments(QuickGame.newInstance(),"QuickGame",returnGameType());
        }
        else if(isFragment("RecordGame")){
            replaceFragments(RecordGame.newInstance(),"RecordGame",returnGameType());
        }
    }

    private int getCurrentFragments(){
        if (isFragment("NormalGame"))return 1;
        else if(isFragment("QuickGame"))return 2;
        else if(isFragment("TotalGame"))return 3;
        else if(isFragment("RecordGame"))return 4;
        else return 0;
    }

    private Fragment returnGameType(){
        Fragment fragment = getFragmentManager().findFragmentById(R.id.gameTypeFragmentContainer);
        if(fragment instanceof Examples)return ExamplesTest.newInstance(getCountPlanet());
        else if(fragment instanceof FindCount)return FindCountTest.newInstance(getCountPlanet());
        else if(fragment instanceof MoreLess)return MoreLessTest.newInstance(getCountPlanet());
        else return null;
    }

    private int returnNumberGameType(){
        Fragment fragment = getFragmentManager().findFragmentById(R.id.gameTypeFragmentContainer);
        if(fragment instanceof FindCount)return 1;
        else if(fragment instanceof Examples){
            if(CreateExample.isIsPlus())return 4;
            else if(CreateExample.isIsMinus())return 5;
            else return 2;
        }
        else if(fragment instanceof MoreLess)return 3;
        else return 0;
    }

    private int getCountPlanet(){
        Fragment currentFragment = PlayPagerActivity.getCurrentFragment();
        if(currentFragment instanceof FirstPlanet)return CountPlanet.getCountFirstPlanet();
        else if(currentFragment instanceof SecondPlanet)return CountPlanet.getCountSecondPlanet();
        else if(currentFragment instanceof ThirdPlanet)return CountPlanet.getCountThirdPlanet();
        else if(currentFragment instanceof FourthPlanet)return CountPlanet.getCountFourthPlanet();
        else if(currentFragment instanceof FifthPlanet)return CountPlanet.getCountFifthPlanet();
        else return 0;
    }

    public String getStringCurrentPlanet(){
        Fragment currentFragment = PlayPagerActivity.getCurrentFragment();
        if(currentFragment instanceof FirstPlanet)return MyDatabase.ONE_PLANET;
        else if(currentFragment instanceof SecondPlanet)return MyDatabase.TWO_PLANET;
        else if(currentFragment instanceof ThirdPlanet)return MyDatabase.THREE_PLANET;
        else if(currentFragment instanceof FourthPlanet)return MyDatabase.FOUR_PLANET;
        else if(currentFragment instanceof FifthPlanet)return MyDatabase.FIVE_PLANET;
        else return "";
    }

    public Planet getCurrentPlanet(){
        Fragment currentFragment = PlayPagerActivity.getCurrentFragment();
        if(currentFragment instanceof FirstPlanet)return FirstPlanet.getFirstPlanet();
        else if(currentFragment instanceof SecondPlanet)return SecondPlanet.getSecondPlanet();
        else if(currentFragment instanceof ThirdPlanet)return ThirdPlanet.getThirdPlanet();
        else if(currentFragment instanceof FourthPlanet)return FourthPlanet.getFourthPlanet();
        else if(currentFragment instanceof FifthPlanet)return FifthPlanet.getFifthPlanet();
        else return null;
    }

    public int getNumberCurrentPlanet(){
        Fragment currentFragment = PlayPagerActivity.getCurrentFragment();
        if(currentFragment instanceof FirstPlanet)return 1;
        else if(currentFragment instanceof SecondPlanet)return 2;
        else if(currentFragment instanceof ThirdPlanet)return 3;
        else if(currentFragment instanceof FourthPlanet)return 4;
        else if(currentFragment instanceof FifthPlanet)return 5;
        else return 0;
    }

    public String getCurrentButton(){
        String currentButton = "";
        if(getNumberCurrentPlanet() != 2) {
            if (returnNumberGameType() == 1) {
                if (getCurrentFragments() == 1) currentButton = MyDatabase.THREE_FIND_BUTTON;
                else if (getCurrentFragments() == 2) currentButton = MyDatabase.TWO_FIND_BUTTON;
                else if (getCurrentFragments() == 3) currentButton = MyDatabase.FOUR_FIND_BUTTON;
            } else if (returnNumberGameType() == 2) {
                if (getCurrentFragments() == 1) currentButton = MyDatabase.THREE_EXAMPLES_BUTTON;
                else if (getCurrentFragments() == 2) currentButton = MyDatabase.TWO_EXAMPLES_BUTTON;
                else if (getCurrentFragments() == 3)
                    currentButton = MyDatabase.FOUR_EXAMPLES_BUTTON;
            } else if (returnNumberGameType() == 3) {
                if (getCurrentFragments() == 1) currentButton = MyDatabase.THREE_MORE_LESS_BUTTON;
                else if (getCurrentFragments() == 2)
                    currentButton = MyDatabase.TWO_MORE_LESS_BUTTON;
                else if (getCurrentFragments() == 3)
                    currentButton = MyDatabase.FOUR_MORE_LESS_BUTTON;
            }
        }else
            {if (returnNumberGameType() == 1) {
            if (getCurrentFragments() == 1) currentButton = MyDatabase.THREE_FIND_BUTTON;
            else if (getCurrentFragments() == 2) currentButton = MyDatabase.TWO_FIND_BUTTON;
            else if (getCurrentFragments() == 3) currentButton = MyDatabase.FOUR_FIND_BUTTON;
        } else if (returnNumberGameType() == 2) {
            if (getCurrentFragments() == 1) currentButton = MyDatabase.FOUR_EXAMPLES_BUTTON;
            else if (getCurrentFragments() == 2) currentButton = MyDatabase.FIVE_EXAMPLES_BUTTON;
            else if (getCurrentFragments() == 3) currentButton = MyDatabase.SIX_EXAMPLES_BUTTON;
        } else if (returnNumberGameType() == 3) {
            if (getCurrentFragments() == 1) currentButton = MyDatabase.THREE_MORE_LESS_BUTTON;
            else if (getCurrentFragments() == 2)
                currentButton = MyDatabase.TWO_MORE_LESS_BUTTON;
            else if (getCurrentFragments() == 3)
                currentButton = MyDatabase.FOUR_MORE_LESS_BUTTON;
        } else if (returnNumberGameType() == 4) {
            if (getCurrentFragments() == 1) currentButton = MyDatabase.TWO_EXAMPLES_BUTTON;
        } else if (returnNumberGameType() == 5) {
            if (getCurrentFragments() == 1) currentButton = MyDatabase.THREE_EXAMPLES_BUTTON;
        }
        }
        return currentButton;
    }

    private void replaceFragments(Fragment gameProgress, String tagProgress, Fragment gameType){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.gameProgressFragmentContainer, gameProgress,tagProgress);
        transaction.replace(R.id.gameTypeFragmentContainer, gameType);
        transaction.addToBackStack(null);
        transaction.commit();}

    public void addAnswerFragment(Fragment fragment, String tag){
        transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.gameAnswerContainer, fragment, tag);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void addFragmentIfTrueAnswer(){
        MyTask.cancel();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                AnswerFragment.isWin = true;
                addAnswerFragment(AnswerFragment.newInstance(), "AnswerFragment");
            }
        };
        Handler h = new Handler();
        h.postDelayed(runnable,800);
    }
    public void addFragmentIfFalseAnswer(){
        MyTask.cancel();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if(!isFragment("AnswerFragment")) {
                    addAnswerFragment(AnswerFragment.newInstance(), "AnswerFragment");
                }
            }
        };
        Handler h = new Handler();
        h.postDelayed(runnable,800);
    }

    public int getCurrentCounterFragment(){
        Fragment fragment = getFragmentManager().findFragmentById(R.id.gameProgressFragmentContainer);
        if(fragment instanceof NormalGame)return NormalGame.getCounterNormalGameFragment();
        else if(fragment instanceof QuickGame)return QuickGame.getCounterQuickGameFragment();
        else if(fragment instanceof TotalGame)return TotalGame.getCounterTotalGameFragment();
        else return 0;
    }

    public void removeProgressAndTypeFragments(){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment1 = getFragmentManager().findFragmentById(R.id.gameProgressFragmentContainer);
        Fragment fragment2 = getFragmentManager().findFragmentById(R.id.gameTypeFragmentContainer);
        transaction.remove(fragment1);
        transaction.remove(fragment2);
        transaction.addToBackStack(null);
        transaction.commit();}

    public void clearStack(){
        int count = getFragmentManager().getBackStackEntryCount();
        while(count > 0){
            getFragmentManager().popBackStack();
            count--;
        }
    }

    public boolean isFragment(String tag){
        if (getFragmentManager() != null) {
            Fragment fragment = getFragmentManager().findFragmentByTag(tag);
            if (fragment != null && fragment.isVisible()) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        if(clickable) {
            if (!isFragment("AnswerFragment")) {
                MyTask.cancel();
                AnswerFragment.isBackPressed = true;
                addAnswerFragment(AnswerFragment.newInstance(), "AnswerFragment");
            }
        }
    }

    public void updateButton(){
        Planet planet = getCurrentPlanet();
        planet.setIsActiveBtn();
    }
}
