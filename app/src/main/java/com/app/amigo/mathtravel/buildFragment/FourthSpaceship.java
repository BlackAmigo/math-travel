package com.app.amigo.mathtravel.buildFragment;

import android.os.Bundle;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;

public class FourthSpaceship extends BuildFragment {

    private static FourthSpaceship fourthSpaceship;

    @Override
    BuildFragment buildFragment() {
        return fourthSpaceship;
    }

    @Override
    public String planet() {
        return MyDatabase.FOUR_PLANET;
    }

    @Override
    int backgroundPlanet() {
        return R.drawable.fon_build_four;
    }

    @Override
    int backgroundScheme() {
        return R.drawable.rocket_four_stars_and_scheme;
    }

    @Override
    public int countParts() {
        return 7;
    }

    @Override
    int rocketPart1() {
        return R.drawable.rocket_four_part1;
    }

    @Override
    int rocketPart2() {
        return R.drawable.rocket_four_part2;
    }

    @Override
    int rocketPart3() {
        return R.drawable.rocket_four_part3;
    }

    @Override
    int rocketPart4() {
        return R.drawable.rocket_four_part4;
    }

    @Override
    int rocketPart5() {
        return R.drawable.rocket_four_part5;
    }

    @Override
    int rocketPart6() {
        return R.drawable.rocket_four_part6;
    }

    @Override
    int rocketPart7() {
        return R.drawable.rocket_four_part7;
    }

    @Override
    void createView(View view) {
        txtPart1.setVisibility(View.VISIBLE);
        txtPart2.setVisibility(View.VISIBLE);
        txtPart3.setVisibility(View.VISIBLE);
        txtPart4.setVisibility(View.VISIBLE);
        txtPart5.setVisibility(View.VISIBLE);
        txtPart6.setVisibility(View.VISIBLE);
        txtPart7.setVisibility(View.VISIBLE);

        txtPart1.setText("10");
        txtPart1.setRotation(270f);
        txtPart2.setText("20");
        txtPart3.setText("30");
        txtPart4.setText("40");
        txtPart5.setText("50");
        txtPart5.setRotation(270f);
        txtPart6.setText("60");
        txtPart6.setRotation(270f);
        txtPart7.setText("70");
        txtPart7.setRotation(270f);
    }

    @Override
    protected void setConstraintParam() {
        constraintSet.setVerticalBias(txtPart1.getId(), 0.770f);
        constraintSet.setHorizontalBias(txtPart1.getId(), 0.43f);
        constraintSet.setVerticalBias(txtPart2.getId(), 0.565f);
        constraintSet.setVerticalBias(txtPart3.getId(), 0.44f);
        constraintSet.setVerticalBias(txtPart4.getId(), 0.280f);
        constraintSet.setHorizontalBias(txtPart4.getId(), 0.5f);
        constraintSet.setVerticalBias(txtPart5.getId(), 0.865f);
        constraintSet.setHorizontalBias(txtPart5.getId(), 0.338f);
        constraintSet.setVerticalBias(txtPart6.getId(), 0.885f);
        constraintSet.setHorizontalBias(txtPart6.getId(), 0.505f);
        constraintSet.setVerticalBias(txtPart7.getId(), 0.865f);
        constraintSet.setHorizontalBias(txtPart7.getId(), 0.66f);
    }

    public static FourthSpaceship getFourthSpaceship() {
        if(fourthSpaceship == null){
            fourthSpaceship = new FourthSpaceship();
            return fourthSpaceship;
        }else return fourthSpaceship;
    }
}
