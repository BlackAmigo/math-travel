package com.app.amigo.mathtravel.buildFragment;

import android.os.Bundle;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.planets.FifthPlanet;

public class FifthSpaceship extends BuildFragment {

    private static FifthSpaceship firstSpaceship;

    @Override
    BuildFragment buildFragment() {
        return firstSpaceship;
    }

    @Override
    public String planet() {
        return MyDatabase.FIVE_PLANET;
    }

    @Override
    int backgroundPlanet() {
        return R.drawable.fon_build_five;
    }

    @Override
    int backgroundScheme() {
        return R.drawable.rocket_five_stars_and_scheme;
    }

    @Override
    public int countParts() {
        return 5;
    }

    @Override
    int rocketPart1() {
        return R.drawable.rocket_five_part1;
    }

    @Override
    int rocketPart2() {
        return R.drawable.rocket_five_part2;
    }

    @Override
    int rocketPart3() {
        return R.drawable.rocket_five_part3;
    }

    @Override
    int rocketPart4() {
        return R.drawable.rocket_five_part4;
    }

    @Override
    int rocketPart5() {
        return R.drawable.rocket_five_part5;
    }

    @Override
    int rocketPart6() {
        return 0;
    }

    @Override
    int rocketPart7() {
        return 0;
    }

    @Override
    void createView(View view) {
        txtPart1.setVisibility(View.VISIBLE);
        txtPart2.setVisibility(View.VISIBLE);
        txtPart3.setVisibility(View.VISIBLE);
        txtPart4.setVisibility(View.VISIBLE);
        txtPart5.setVisibility(View.VISIBLE);

        txtPart1.setText("40");
        txtPart2.setText("20");
        txtPart2.setRotation(270f);
        txtPart3.setText("30");
        txtPart3.setRotation(270f);
        txtPart4.setText("10");
        txtPart4.setRotation(270f);
        txtPart5.setText("50");
    }

    @Override
    protected void setConstraintParam() {
        constraintSet.setVerticalBias(txtPart4.getId(), 0.8f);
        constraintSet.setHorizontalBias(txtPart4.getId(), 0.35f);
        constraintSet.setVerticalBias(txtPart2.getId(), 0.700f);
//        constraintSet.setHorizontalBias(txtPart2.getId(), 0.512f);
        constraintSet.setVerticalBias(txtPart3.getId(), 0.81f);
        constraintSet.setHorizontalBias(txtPart3.getId(), 0.66f);
        constraintSet.setVerticalBias(txtPart1.getId(), 0.40f);
//        constraintSet.setHorizontalBias(txtPart4.getId(), 0.5f);
        constraintSet.setVerticalBias(txtPart5.getId(), 0.230f);
//        constraintSet.setHorizontalBias(txtPart5.getId(), 0.5f);

    }

    public static FifthSpaceship getFifthSpaceship() {
        if(firstSpaceship == null){
            firstSpaceship = new FifthSpaceship();
            return firstSpaceship;
        } else return firstSpaceship;
    }
}
