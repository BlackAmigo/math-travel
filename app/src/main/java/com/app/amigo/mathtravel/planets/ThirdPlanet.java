package com.app.amigo.mathtravel.planets;

import android.support.v4.app.Fragment;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.buildFragment.ThirdSpaceship;
import com.app.amigo.mathtravel.database.MyDatabase;

public class ThirdPlanet extends Planet {

    private static ThirdPlanet thirdPlanet;

    private ThirdSpaceship thirdSpaceship = ThirdSpaceship.getThirdSpaceship();

    @Override
    int layoutPlanet() {
        return R.layout.fragment_planet_three;
    }

    @Override
    int getCountPlanet() {
        return CountPlanet.getCountThirdPlanet();
    }

    @Override
    void initButtons(View view) {
        initFindCountBtn(view);
        initExamplesBtn(view,false);
        initMoreLessBtn(view);
    }

    @Override
    public void setIsActiveBtn() {
        initIsActiveBtn(MyDatabase.THREE_PLANET, MyDatabase.BUTTONS);
    }

    @Override
    Fragment buildFragment() {
        return ThirdSpaceship.getThirdSpaceship();
    }

    @Override
    int spaceship() {
        if(myDbHelper.allParts(thirdSpaceship)){
            return R.drawable.rocket_three_with_flame;
        }else  return R.drawable.rocket_three_scheme;
    }

    public static ThirdPlanet getThirdPlanet() {
        if(thirdPlanet == null){
            thirdPlanet = new ThirdPlanet();
            return thirdPlanet;
        }else {
            return thirdPlanet;
        }
    }
}
