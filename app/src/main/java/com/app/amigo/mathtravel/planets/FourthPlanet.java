package com.app.amigo.mathtravel.planets;


import android.support.v4.app.Fragment;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.buildFragment.FourthSpaceship;
import com.app.amigo.mathtravel.database.MyDatabase;

public class FourthPlanet extends Planet {

    private static FourthPlanet fourthPlanet;

    private FourthSpaceship fourthSpaceship = FourthSpaceship.getFourthSpaceship();

    @Override
    int layoutPlanet() {
        return R.layout.fragment_planet_four;
    }

    @Override
    int getCountPlanet() {
        return CountPlanet.getCountFourthPlanet();
    }

    @Override
    void initButtons(View view) {
        initFindCountBtn(view);
        initExamplesBtn(view,false);
    }

    @Override
    public void setIsActiveBtn() {
        initIsActiveBtn(MyDatabase.FOUR_PLANET, MyDatabase.BUTTONS);
    }

    @Override
    Fragment buildFragment() {
        return FourthSpaceship.getFourthSpaceship();
    }

    @Override
    int spaceship() {
        if(myDbHelper.allParts(fourthSpaceship)){
            return R.drawable.rocket_four_with_flame;
        }else  return R.drawable.rocket_four_scheme;
    }

    public static FourthPlanet getFourthPlanet() {
        if(fourthPlanet == null){
            fourthPlanet = new FourthPlanet();
            return fourthPlanet;
        }else {
            return fourthPlanet;
        }
    }
}
