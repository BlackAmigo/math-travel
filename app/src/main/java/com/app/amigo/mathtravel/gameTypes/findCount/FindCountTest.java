package com.app.amigo.mathtravel.gameTypes.findCount;

import android.os.Bundle;

public class FindCountTest extends FindCount{

    private static int planetCount;

    @Override
    public int endCount() {
        return planetCount;
    }

    public static FindCountTest newInstance(int getCountPlanet) {
        planetCount = getCountPlanet;
        Bundle args = new Bundle();
        FindCountTest fragment = new FindCountTest();
        fragment.setArguments(args);
        return fragment;
    }
}
