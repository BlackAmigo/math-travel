package com.app.amigo.mathtravel.gameTypes.examples;

import com.app.amigo.mathtravel.game.HelperTypes;

import java.util.Random;

public abstract class CreateExample extends HelperTypes {

    private String example;
    private int rightAnswer;
    private int wrongAnswer1, wrongAnswer2, wrongAnswer3;
    protected int[] getAnswers;
    protected static boolean isPlus = false;
    protected static boolean isMinus = false;
    private boolean mBool;
    protected abstract int count();

    public int getRightAnswer() {
        return rightAnswer;
    }

    public static void setIsPlus(boolean isPlus) {
        CreateExample.isPlus = isPlus;
    }

    public static void setIsMinus(boolean isMinus) {
        CreateExample.isMinus = isMinus;
    }

    public static boolean isIsPlus() {
        return isPlus;
    }

    public static boolean isIsMinus() {
        return isMinus;
    }

    private void randomExample (){

        int a = (int)(Math.random()*count());
        int b = (int)(Math.random()*count());
        int c = (int)(Math.random()*count());

        Random r = new Random();

        if(isPlus)mBool = true;
        else if(isMinus)mBool = false;
        else mBool = r.nextBoolean();

        if (mBool) {
            example = a + " + " + b + " = ";
            rightAnswer = a + b;
            wrongAnswer1 = a + (b/2);
            wrongAnswer2 = (a/2) + b;
            wrongAnswer3 = a + c;


        } else{
            do {
                a = (int)(Math.random()*count());
                b = (int)(Math.random()*count());
            }
            while (a<b | b < 2);
            example = a + " - " + b + " = ";
            rightAnswer = a - b;
            wrongAnswer1 = a - (b/2);
            wrongAnswer2 = (a/2) - b;
            wrongAnswer3 = (a/2) - (b/2);
        }
        getAnswers = new int[]{rightAnswer, wrongAnswer1, wrongAnswer2, wrongAnswer3};
    }

    protected String getExample(){
        randomExample();
        return example;
    }


}
