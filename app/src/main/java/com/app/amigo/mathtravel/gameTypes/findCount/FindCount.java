package com.app.amigo.mathtravel.gameTypes.findCount;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.game.HelperTypes;
import com.app.amigo.mathtravel.gameProgress.GameProgressFragment;

import java.util.ArrayList;
import java.util.Collections;

public abstract class FindCount extends HelperTypes {

        public static final String TAG = "FindCount";
        private static Integer num = -1;
        public abstract int endCount();


    @Nullable
        @Override
        public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
            View view = inflater.inflate(R.layout.game_find,container,false);
            Log.i(TAG,"onCreateView !!!");

            GridLayout gridLayout = (GridLayout) view.findViewById(R.id.gridLayout);

            createButtons(6, 6, endCount(),gridLayout);

            createText(view);

            clickableDelay();

            return view;

        }

        public  void createText(View view){
            TextView texFindNumber = view.findViewById(R.id.textFindNumber);
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        texFindNumber.setText(String.valueOf(num));
                    }
                };
                Handler h = new Handler();
                h.postDelayed(runnable,10);
        }


        public void createButtons(final int columns, final int rows, final int endCount, final GridLayout field) {
            // post action надо, потому что дальше в коде наше поле делаем квадратным
            field.post(new Runnable() {
                @Override
                public void run() {
                    ViewGroup.LayoutParams fieldParams = field.getLayoutParams(); // берем параметры поля (лаяута)
                    int size = java.lang.Math.min(field.getWidth(), field.getHeight()); // определяем меньшую сторону
                    fieldParams.width = size; // делаем стороны одинаковыми, квадрат
                    fieldParams.height = size;
                    field.setLayoutParams(fieldParams); // применяем новые параметры
                    field.setColumnCount(columns); // задаем полю количество столбцов
                    field.setRowCount(rows); // и строк
//                    field.setBackgroundResource(R.drawable.rounded_corners);

                    ArrayList<Integer> array = generatorCounts(columns,rows,endCount);

                    int i = 0;
                    // цикл в котором создаются кнопки
                    for (int r = 0; r < rows; r++) {
                        for (int c = 0; c < columns; c++) {
                            Button btn = new Button(field.getContext());
                            btn.setOnClickListener(listener); // вешаем слушателя

                            btn.setTag(c + " " + r); // назначаем tag на вьюшку, пригодится когда надо будет определить позицию на поле
                            btn.setTextSize(18); // это можно убрать

                            if(array.get(i) == -1) {
                                //  btn.setText("-1");
                                //  btn.setVisibility(View.GONE);
                                btn.setBackgroundResource(R.drawable.buttons_transparent_style);

                            }else {
                                btn.setText(array.get(i).toString());
                                btn.setBackgroundResource(R.drawable.grid_buttons_style);
                            }
                            i++;

                            GridLayout.LayoutParams lp = new GridLayout.LayoutParams(); // создаем параметры лаяута для кнопки

                            lp.width = 0; // так надо потому что ниже мы указываем вес кнопок = 1, они будут сами высчитывать размеры
                            lp.height = 0;
                            lp.columnSpec = GridLayout.spec(c, 1f); // вес и позиция кнопки по горизонтали
                            lp.rowSpec = GridLayout.spec(r, 1f); // и по вертикали
                            field.addView(btn, lp); // добавляем кнопку на поле
                        }
                    }
                }
            });
        }

        private final View.OnClickListener listener = new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Button b = (Button) v;
                String txt = b.getText().toString();

                if(!txt.equals("")){

                    if (clickable) {
                        clickable = false;

                        int i = Integer.parseInt(txt);
                        if (i == num) {
                            Log.i(TAG, "onClick");
                            b.setBackground(getResources().getDrawable(R.drawable.grid_buttons_green_style));
                            if(getCurrentCounterFragment() > 9){
                                GameProgressFragment.l10.setBackgroundResource(R.drawable.progress_bar_style_green);
                                addFragmentIfTrueAnswer();
                                mMyDbHelper.updateIsActiveButton(getStringCurrentPlanet(),getCurrentButton(),1);
                                updateButton();
                            }else {replaceFragmentsIfRightAnswer();}
                        } else if (i != num){
                            b.setBackground(getResources().getDrawable(R.drawable.grid_buttons_red_style));
                            addFragmentIfFalseAnswer();
                        }
                    }
                }
            }
        };

        private   ArrayList<Integer> generatorCounts (int columns, int rows, int endCount){
            int total = columns * rows;

            if(total >= endCount){
              ArrayList<Integer> arr = new ArrayList<>();
                for ( int i = 0; i < total; i++ ) {
                    if(i<=endCount)arr.add(i);
                 else arr.add(-1);
                }
                int i;
                do {
                    i = (int) (java.lang.Math.random() * endCount);
                }
                while (num == i);
                num = i;
                Collections.shuffle(arr);
            return arr;}

            else {
                ArrayList<Integer> arrayEndCount = new ArrayList();
                for (int i = 0; i < endCount; i++) {
                    arrayEndCount.add(i);
                }
                Collections.shuffle(arrayEndCount);

                ArrayList<Integer> arr = new ArrayList<>();
                for (int i = 0; i < total; i++) {
                    arr.add(arrayEndCount.get(i));
                }
                int r = (int) (java.lang.Math.random() * arr.size());
                num = arr.get(r);
                return arr;
            }
        }

        @Override
        public void onPause() {
            super.onPause();
            Log.i(TAG,"onPause !!!");
        }

        @Override
        public void onStop() {
            super.onStop();
            Log.i(TAG,"onStop !!!");
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            Log.i(TAG,"onDestroyView !!!");
        }
    }