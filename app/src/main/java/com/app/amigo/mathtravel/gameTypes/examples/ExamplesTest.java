package com.app.amigo.mathtravel.gameTypes.examples;

import android.os.Bundle;

public class ExamplesTest extends Examples {

    private static int planetCount;

    @Override
    protected int count() {
        return planetCount;
    }

    public static ExamplesTest newInstance(int getCountPlanet) {
        planetCount = getCountPlanet;
        Bundle args = new Bundle();
        ExamplesTest fragment = new ExamplesTest();
        fragment.setArguments(args);
        return fragment;
    }
}
