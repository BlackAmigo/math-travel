package com.app.amigo.mathtravel.buildFragment;

import android.os.Bundle;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.planets.FirstPlanet;

public class FirstSpaceship extends BuildFragment implements View.OnClickListener {

    private static FirstSpaceship firstSpaceship;

    public static void setImageSpaceship(){                    //   ???????????????
        FirstPlanet.getFirstPlanet().setImageSpaceship();
    }

    @Override
    BuildFragment buildFragment() {
        return firstSpaceship;
    }

    @Override
   public String planet() {
        return MyDatabase.ONE_PLANET;
    }

    @Override
    int backgroundPlanet() {
        return R.drawable.fon_build_one;
    }

    @Override
    int backgroundScheme() {
        return R.drawable.rocket_one_stars_and_scheme;
    }

    @Override
    public int countParts() {
        return 4;
    }

    @Override
    int rocketPart1() {
        return R.drawable.rocket_one_part1;

    }

    @Override
    int rocketPart2() {
        return R.drawable.rocket_one_part2;

    }

    @Override
    int rocketPart3() {
        return R.drawable.rocket_one_part3;

    }

    @Override
    int rocketPart4() {
        return R.drawable.rocket_one_part4;

    }

    @Override
    int rocketPart5() {
        return 0;
    }

    @Override
    int rocketPart6() {
        return 0;
    }

    @Override
    int rocketPart7() {
        return 0;
    }

    @Override
    void createView(View view) {
        txtPart1.setVisibility(View.VISIBLE);
        txtPart2.setVisibility(View.VISIBLE);
        txtPart3.setVisibility(View.VISIBLE);
        txtPart4.setVisibility(View.VISIBLE);

        txtPart1.setText("10");
        txtPart2.setText("20");
        txtPart3.setText("40");
        txtPart4.setText("30");
    }

    @Override
    protected void setConstraintParam() {
        constraintSet.setVerticalBias(txtPart1.getId(), 0.840f);
        constraintSet.setHorizontalBias(txtPart1.getId(), 0.512f);
        constraintSet.setVerticalBias(txtPart2.getId(), 0.5f);
        constraintSet.setHorizontalBias(txtPart2.getId(), 0.512f);
        constraintSet.setVerticalBias(txtPart3.getId(), 0.630f);
        constraintSet.setHorizontalBias(txtPart3.getId(), 0.35f);
        constraintSet.setVerticalBias(txtPart4.getId(), 0.310f);
        constraintSet.setHorizontalBias(txtPart4.getId(), 0.512f);

    }

    public static FirstSpaceship getFirstSpaceShip() {
        if(firstSpaceship == null){
            firstSpaceship = new FirstSpaceship();
            return firstSpaceship;
        } else return firstSpaceship;
    }


}
