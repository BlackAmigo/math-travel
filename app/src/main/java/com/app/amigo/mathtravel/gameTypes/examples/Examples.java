package com.app.amigo.mathtravel.gameTypes.examples;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.gameProgress.GameProgressFragment;
import com.app.amigo.mathtravel.planets.Planet;

import java.util.Arrays;
import java.util.Collections;

public abstract class Examples extends CreateExample implements View.OnClickListener {

    private TextView txtExample;
    private TextView txtAnswerExample;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_examples,container,false);

        txtExample = view.findViewById(R.id.textExample);
        txtAnswerExample = view.findViewById(R.id.textAnswerExample);
        txtExample.setText(getExample());
        txtAnswerExample.setText(String.valueOf(getRightAnswer()));
        txtAnswerExample.setVisibility(View.INVISIBLE);
        initButtons(view);
        clickableDelay();
        return view;
    }


    private void initButtons(View view){
        Button btn1 = view.findViewById(R.id.btn1);
        Button btn2 = view.findViewById(R.id.btn2);
        Button btn3 = view.findViewById(R.id.btn3);
        Button btn4 = view.findViewById(R.id.btn4);

        Button [] buttons = {btn1, btn2, btn3, btn4};

        Collections.shuffle(Arrays.asList(buttons));

        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setText(String.valueOf(getAnswers[i]));
        }

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if (clickable) {
            clickable = false;

           Button b = (Button) v;
           String txt = b.getText().toString();
           int i = Integer.parseInt(txt);

           if(i == getRightAnswer()){
               txtAnswerExample.setText(String.valueOf(getRightAnswer()));
               txtAnswerExample.setVisibility(View.VISIBLE);
               setTextColor(true,txtAnswerExample,txtExample);
               if(getCurrentCounterFragment() > 9){
                   GameProgressFragment.l10.setBackgroundResource(R.drawable.progress_bar_style_green);
                   addFragmentIfTrueAnswer();
                   mMyDbHelper.updateIsActiveButton(getStringCurrentPlanet(),getCurrentButton(),1);
                   updateButton();
               }else {replaceFragmentsIfRightAnswer();}

                } else if (i != getRightAnswer()){
                      setTextColor(false,txtAnswerExample,txtExample);
                      addFragmentIfFalseAnswer();
           }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isPlus = false;
        isMinus = false;
    }
}
