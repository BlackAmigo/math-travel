package com.app.amigo.mathtravel.buildFragment;

import android.os.Bundle;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.planets.SecondPlanet;

public class SecondSpaceship extends  BuildFragment {

    private static SecondSpaceship secondSpaceship;

    @Override
    BuildFragment buildFragment() {
        return secondSpaceship;
    }

    @Override
    public String planet() {
        return MyDatabase.TWO_PLANET;
    }

    @Override
    int backgroundPlanet() {
        return R.drawable.fon_build_two;
    }

    @Override
    int backgroundScheme() {
        return R.drawable.rocket_two_stars_and_scheme;
    }

    @Override
    public int countParts() {
        return 5;
    }

    @Override
    int rocketPart1() {
        return R.drawable.rocket_two_part1;
    }

    @Override
    int rocketPart2() {
        return R.drawable.rocket_two_part2;
    }

    @Override
    int rocketPart3() {
        return R.drawable.rocket_two_part3;
    }

    @Override
    int rocketPart4() {
        return R.drawable.rocket_two_part4;
    }

    @Override
    int rocketPart5() {
        return R.drawable.rocket_two_part5;
    }

    @Override
    int rocketPart6() {
        return 0;
    }

    @Override
    int rocketPart7() {
        return 0;
    }

    @Override
    void createView(View view) {
        txtPart1.setVisibility(View.VISIBLE);
        txtPart2.setVisibility(View.VISIBLE);
        txtPart3.setVisibility(View.VISIBLE);
        txtPart4.setVisibility(View.VISIBLE);
        txtPart5.setVisibility(View.VISIBLE);

        txtPart1.setText("10");
        txtPart2.setText("20");
        txtPart3.setText("30");
        txtPart4.setText("40");
        txtPart5.setText("50");
    }

    @Override
    protected void setConstraintParam() {
        constraintSet.setVerticalBias(txtPart1.getId(), 0.70f);
        constraintSet.setVerticalBias(txtPart2.getId(), 0.52f);
        constraintSet.setVerticalBias(txtPart3.getId(), 0.250f);
        constraintSet.setVerticalBias(txtPart4.getId(), 0.730f);
        constraintSet.setHorizontalBias(txtPart4.getId(), 0.32f);
        constraintSet.setVerticalBias(txtPart5.getId(), 0.730f);
        constraintSet.setHorizontalBias(txtPart5.getId(), 0.67f);
    }

    public static SecondSpaceship getSecondSpaceship() {
        if(secondSpaceship == null){
            secondSpaceship = new SecondSpaceship();
            return secondSpaceship;
        }else return secondSpaceship;
    }
}
