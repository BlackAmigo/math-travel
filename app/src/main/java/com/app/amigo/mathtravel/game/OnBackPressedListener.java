package com.app.amigo.mathtravel.game;

public interface OnBackPressedListener {
    void onBackPressed();
}
