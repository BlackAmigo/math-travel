package com.app.amigo.mathtravel.planets;

import android.support.v4.app.Fragment;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.buildFragment.FifthSpaceship;
import com.app.amigo.mathtravel.database.MyDatabase;

public class FifthPlanet extends Planet {

    private static FifthPlanet fifthPlanet;

    private FifthSpaceship fifthSpaceship = FifthSpaceship.getFifthSpaceship();

    @Override
    int layoutPlanet() {
        return R.layout.fragment_planet_five;
    }

    @Override
    int getCountPlanet() {
        return CountPlanet.getCountFifthPlanet();
    }

    @Override
    void initButtons(View view) {
        initFindCountBtn(view);
        initExamplesBtn(view,false);

    }

    @Override
    public void setIsActiveBtn() {
        initIsActiveBtn(MyDatabase.FIVE_PLANET, MyDatabase.BUTTONS);
    }

    @Override
    Fragment buildFragment() {
        return FifthSpaceship.getFifthSpaceship();
    }

    @Override
    int spaceship() {
        if(myDbHelper.allParts(fifthSpaceship)){
            return R.drawable.rocket_five_with_flame;
        }else  return R.drawable.rocket_five_scheme;
    }

    public static FifthPlanet getFifthPlanet() {
        if(fifthPlanet == null){
            fifthPlanet = new FifthPlanet();
            return fifthPlanet;
        }else {
            return fifthPlanet;
        }
    }
}
