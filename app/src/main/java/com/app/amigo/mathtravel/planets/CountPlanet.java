package com.app.amigo.mathtravel.planets;

public class CountPlanet {

    public static int getCountFirstPlanet() {
        return 5;
    }

    public static int getCountSecondPlanet() {
        return 10;
    }

    public static int getCountThirdPlanet() {
        return 100;
    }

    public static int getCountFourthPlanet() {
        return 500;
    }

    public static int getCountFifthPlanet() {
        return 1000;
    }
}
