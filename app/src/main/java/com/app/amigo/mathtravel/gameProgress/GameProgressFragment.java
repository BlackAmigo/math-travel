package com.app.amigo.mathtravel.gameProgress;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.game.AnswerFragment;
import com.app.amigo.mathtravel.game.HelperTypes;

import java.util.concurrent.TimeUnit;

public abstract class GameProgressFragment extends HelperTypes {

    private static final String TAG = "GameProgressFragment";

    protected static boolean isTotal;
    private boolean isPause;

    private ProgressBar progressTimeBar;
    private static int progress;
    public static LinearLayout l10;

    abstract int counterFragment();
    abstract long timeOut();
    abstract void createView(View view);

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.game_progress,container,false);
        Log.i(TAG, "onCreateView");

        createView(view);
//        PlayPagerActivity.playPagerToolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
        return view;
    }

    protected void createProgressBar(boolean isVisibility, View view){
        if(isVisibility) {
            LinearLayout l1 = view.findViewById(R.id.progress_bar_1);
            LinearLayout l2 = view.findViewById(R.id.progress_bar_2);
            LinearLayout l3 = view.findViewById(R.id.progress_bar_3);
            LinearLayout l4 = view.findViewById(R.id.progress_bar_4);
            LinearLayout l5 = view.findViewById(R.id.progress_bar_5);
            LinearLayout l6 = view.findViewById(R.id.progress_bar_6);
            LinearLayout l7 = view.findViewById(R.id.progress_bar_7);
            LinearLayout l8 = view.findViewById(R.id.progress_bar_8);
            LinearLayout l9 = view.findViewById(R.id.progress_bar_9);
             l10 = view.findViewById(R.id.progress_bar_10);

            l1.setBackgroundResource(R.drawable.progress_bar_style);
            l2.setBackgroundResource(R.drawable.progress_bar_style);
            l3.setBackgroundResource(R.drawable.progress_bar_style);
            l4.setBackgroundResource(R.drawable.progress_bar_style);
            l5.setBackgroundResource(R.drawable.progress_bar_style);
            l6.setBackgroundResource(R.drawable.progress_bar_style);
            l7.setBackgroundResource(R.drawable.progress_bar_style);
            l8.setBackgroundResource(R.drawable.progress_bar_style);
            l9.setBackgroundResource(R.drawable.progress_bar_style);
            l10.setBackgroundResource(R.drawable.progress_bar_style);

            if (counterFragment() == 1) l1.setBackgroundResource(R.drawable.progress_bar_style_red);
            else if (counterFragment() > 1)
                l1.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 2) {l2.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 2)
                l2.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 3) {l3.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 3)
                l3.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 4) {l4.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 4)
                l4.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 5) {l5.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 5)
                l5.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 6) {l6.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 6)
                l6.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 7) {l7.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 7)
                l7.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 8) {l8.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 8)
                l8.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 9) {l9.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 9)
                l9.setBackgroundResource(R.drawable.progress_bar_style_green);
            if (counterFragment() == 10) {l10.setBackgroundResource(R.drawable.progress_bar_style_red);
                updateGold();}
            else if (counterFragment() > 10)
                l10.setBackgroundResource(R.drawable.progress_bar_style_green);
        }else {
            LinearLayout progressbar = view.findViewById(R.id.linearLayout2);
            progressbar.setVisibility(View.INVISIBLE);
        }
    }

    protected void createTimeProgressBar(boolean isVisibility, View view){
        progressTimeBar = view.findViewById(R.id.include_progress_time_bar);
        if(isVisibility) {
            if(!isTotal){
            progress = progressTimeBar.getMax();
            startMyBarTask(500);}
            if(isTotal){
             if(TotalGame.getCounterTotalGameFragment() <= 1)progress = progressTimeBar.getMax();
                startMyBarTask(500);
            }
        }else progressTimeBar.setVisibility(View.GONE);
    }

    public void createRecordLayout(boolean isVisibility, View view){
        LinearLayout recordLayout = view.findViewById(R.id.record_layout);
        if(isVisibility) {
            TextView txtRecord = view.findViewById(R.id.text_record_count);
            txtRecord.setText(String.valueOf(RecordGame.getCounterRecordGameFragment()-1));
            if(RecordGame.getCounterRecordGameFragment() > 1){
                updateFuel();
            }
        }else recordLayout.setVisibility(View.GONE);
    }

    protected void startMyBarTask(long delayMillis) {
        MyTask.setMt(new MyBarTask());
        progressTimeBar.setProgress(progress);
        Runnable r = new Runnable() {
            @Override
            public void run() {
                MyTask.execute(progress);
            }
        };
        Handler h = new Handler();
        h.postDelayed(r,delayMillis);
    }


    class MyBarTask extends AsyncTask<Integer,Integer,Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            Log.i(TAG, "onPreExecute");
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.i(TAG, "onPostExecute");
            progress = progressTimeBar.getMax();

            if(!isFragment("AnswerFragment")){
                    MyTask.cancel();
                    AnswerFragment.isBackPressed = false;
                    AnswerFragment.isTimeOut = true;
                addAnswerFragment(AnswerFragment.newInstance(), "AnswerFragment");
            }
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
//            Log.i(TAG, "onProgressUpdate");

            progressTimeBar.setProgress(values[0]);
        }

        @Override
        protected Void doInBackground(Integer... num) {
            try {
                for (int i = num[0]; i >= 0; i--) {

                    progress = i;
                    if (isCancelled()) return null;

                    publishProgress(progress);

                    TimeUnit.MICROSECONDS.sleep(timeOut());  //  timeOut 3000

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            Log.i(TAG, "onCancelled");
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
        MyTask.cancel();
        isPause=true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
//        PlayPagerActivity.playPagerToolbar.setBackgroundColor(getResources().getColor(R.color.toolbar));
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
        if(isPause) {
            if (!isFragment("AnswerFragment")) {
                AnswerFragment.isBackPressed = true;
                addAnswerFragment(AnswerFragment.newInstance(), "AnswerFragment");
                isPause=false;
            }
        }
    }
}
