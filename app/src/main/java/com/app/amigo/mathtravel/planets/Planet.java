package com.app.amigo.mathtravel.planets;

import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.database.MyDbHelper;
import com.app.amigo.mathtravel.game.IsActiveButtonListener;
import com.app.amigo.mathtravel.gameProgress.NormalGame;
import com.app.amigo.mathtravel.gameProgress.QuickGame;
import com.app.amigo.mathtravel.gameProgress.RecordGame;
import com.app.amigo.mathtravel.gameProgress.TotalGame;
import com.app.amigo.mathtravel.gameTypes.examples.CreateExample;
import com.app.amigo.mathtravel.gameTypes.examples.ExamplesTest;
import com.app.amigo.mathtravel.gameTypes.findCount.FindCountTest;
import com.app.amigo.mathtravel.gameTypes.moreLess.MoreLessTest;

public abstract class Planet extends Fragment implements  View.OnClickListener, IsActiveButtonListener {

    private static final String TAG = "Planet";
    protected MyDbHelper myDbHelper;
    abstract int layoutPlanet();
    abstract int getCountPlanet();
    abstract void initButtons(View view);
//    abstract void setIsActiveBtn();
    abstract int spaceship();
    abstract Fragment buildFragment();
    private ImageView imageSpaceship;
    protected AppCompatImageButton mOneFindCountButton, mTwoFindCountButton, mThreeFindCountButton, mFourFindCountButton,
            mOneExamplesButton, mTwoExamplesButton, mThreeExamplesButton, mFourExamplesButton, mFiveExamplesButton, mSixExamplesButton,
            mOneMoreLessButton, mTwoMoreLessButton, mThreeMoreLessButton, mFourMoreLessButton;

    protected AppCompatImageButton [] btnArr;

    private void setButtonStyle(AppCompatImageButton button, @DrawableRes int drawableRes, @DrawableRes int drawableResSmall, @DrawableRes int drawableResInactive){
        if (button != null) {

            VectorDrawableCompat vcAccept = VectorDrawableCompat.create(getResources(), drawableRes, null);
            VectorDrawableCompat vcAcceptSmall = VectorDrawableCompat.create(getResources(), drawableResSmall, null);
            VectorDrawableCompat vcAcceptInactive = VectorDrawableCompat.create(getResources(), drawableResInactive, null);

            StateListDrawable stateList = new StateListDrawable();
            stateList.addState(new int[]{android.R.attr.state_focused, -android.R.attr.state_pressed}, vcAccept);
            stateList.addState(new int[]{android.R.attr.state_focused, android.R.attr.state_pressed}, vcAcceptSmall);
            stateList.addState(new int[]{-android.R.attr.state_focused, android.R.attr.state_pressed}, vcAcceptSmall);
            stateList.addState(new int[]{-android.R.attr.state_enabled},vcAcceptInactive);
            stateList.addState(new int[]{}, vcAccept);

            button.setImageDrawable(stateList);
            button.setPadding(0,0,0,0);
            button.setBackgroundColor(Color.TRANSPARENT);
            button.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }

    protected void initFindCountBtn(View view){
        mOneFindCountButton = view.findViewById(R.id.buttonOne4FindCount);
        setButtonStyle(mOneFindCountButton, R.drawable.btn_find_normal, R.drawable.btn_find_normal_small, R.drawable.btn_inactiv_round_find);

        mTwoFindCountButton = view.findViewById(R.id.buttonTwo4FindCount);
        setButtonStyle(mTwoFindCountButton, R.drawable.btn_find_total, R.drawable.btn_find_total_small, R.drawable.btn_inactiv_find);

        mThreeFindCountButton = view.findViewById(R.id.buttonThree4FindCount);
        setButtonStyle(mThreeFindCountButton, R.drawable.btn_find_quick, R.drawable.btn_find_quick_small, R.drawable.btn_inactiv_find);

        mFourFindCountButton = view.findViewById(R.id.buttonFour4FindCount);
        setButtonStyle(mFourFindCountButton, R.drawable.btn_find_record, R.drawable.btn_find_record_small, R.drawable.btn_inactiv_find);

        mOneFindCountButton.setOnClickListener(this);
        mTwoFindCountButton.setOnClickListener(this);
        mThreeFindCountButton.setOnClickListener(this);
        mFourFindCountButton.setOnClickListener(this);

    }

    protected void initExamplesBtn(View view, boolean isSixBtn){
        if(isSixBtn){
            mOneExamplesButton = view.findViewById(R.id.buttonOne6Examples);
            setButtonStyle(mOneExamplesButton, R.drawable.btn_examples_plus, R.drawable.btn_examples_plus_small, R.drawable.btn_inactiv_round_plus);

            mTwoExamplesButton = view.findViewById(R.id.buttonTwo6Examples);
            setButtonStyle(mTwoExamplesButton, R.drawable.btn_examples_minus, R.drawable.btn_examples_minus_small, R.drawable.btn_inactiv_round_minus);

            mThreeExamplesButton = view.findViewById(R.id.buttonThree6Examples);
            setButtonStyle(mThreeExamplesButton,R.drawable.btn_examples_normal, R.drawable.btn_examples_normal_small, R.drawable.btn_inactiv_round_examples);

            mFourExamplesButton = view.findViewById(R.id.buttonFour6Examples);
            setButtonStyle(mFourExamplesButton, R.drawable.btn_examples_quick, R.drawable.btn_examples_quick_small, R.drawable.btn_inactiv_examples);

            mFiveExamplesButton = view.findViewById(R.id.buttonFive6Examples);
            setButtonStyle( mFiveExamplesButton, R.drawable.btn_examples_total, R.drawable.btn_examples_total_small, R.drawable.btn_inactiv_examples);

            mSixExamplesButton = view.findViewById(R.id.buttonSix6Examples);
            setButtonStyle(mSixExamplesButton, R.drawable.btn_examples_record, R.drawable.btn_examples_record_small, R.drawable.btn_inactiv_examples);

            mOneExamplesButton.setOnClickListener(this);
            mTwoExamplesButton.setOnClickListener(this);
            mThreeExamplesButton.setOnClickListener(this);
            mFourExamplesButton.setOnClickListener(this);
            mFiveExamplesButton.setOnClickListener(this);
            mSixExamplesButton.setOnClickListener(this);
        }else {
            mOneExamplesButton = view.findViewById(R.id.buttonOne4Examples);
            setButtonStyle(mOneExamplesButton, R.drawable.btn_examples_normal, R.drawable.btn_examples_normal_small, R.drawable.btn_inactiv_round_examples);

            mTwoExamplesButton = view.findViewById(R.id.buttonTwo4Examples);
            setButtonStyle(mTwoExamplesButton, R.drawable.btn_examples_total, R.drawable.btn_examples_total_small, R.drawable.btn_inactiv_examples);

            mThreeExamplesButton = view.findViewById(R.id.buttonThree4Examples);
            setButtonStyle(mThreeExamplesButton,R.drawable.btn_examples_quick, R.drawable.btn_examples_quick_small, R.drawable.btn_inactiv_examples);

            mFourExamplesButton = view.findViewById(R.id.buttonFour4Examples);
            setButtonStyle(mFourExamplesButton, R.drawable.btn_examples_record, R.drawable.btn_examples_record_small, R.drawable.btn_inactiv_examples);

            mOneExamplesButton.setOnClickListener(this);
            mTwoExamplesButton.setOnClickListener(this);
            mThreeExamplesButton.setOnClickListener(this);
            mFourExamplesButton.setOnClickListener(this);
        }
    }

    protected void initMoreLessBtn(View view){
        mOneMoreLessButton = view.findViewById(R.id.buttonOne4MoreLess);
        setButtonStyle(mOneMoreLessButton, R.drawable.btn_more_less_normal, R.drawable.btn_more_less_normal_small, R.drawable.btn_inactiv_round_more_less);

        mTwoMoreLessButton = view.findViewById(R.id.buttonTwo4MoreLess);
        setButtonStyle(mTwoMoreLessButton, R.drawable.btn_more_less_total, R.drawable.btn_more_less_total_small, R.drawable.btn_inactiv_more_less);

        mThreeMoreLessButton = view.findViewById(R.id.buttonThree4MoreLess);
        setButtonStyle(mThreeMoreLessButton, R.drawable.btn_more_less_quick, R.drawable.btn_more_less_quick_small, R.drawable.btn_inactiv_more_less);

        mFourMoreLessButton = view.findViewById(R.id.buttonFour4MoreLess);
        setButtonStyle(mFourMoreLessButton, R.drawable.btn_more_less_record, R.drawable.btn_more_less_record_small, R.drawable.btn_inactiv_more_less);

        mOneMoreLessButton.setOnClickListener(this);
        mTwoMoreLessButton.setOnClickListener(this);
        mThreeMoreLessButton.setOnClickListener(this);
        mFourMoreLessButton.setOnClickListener(this);
    }

    @Override
    public void onClick (View view) {
        Log.i(TAG,"onClick");

        switch (view.getId()){
            case (R.id.imageSpaceshipContainer) :
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.build_container, buildFragment());
                transaction.addToBackStack(null);
                transaction.commit();
                break;
//                                            "Find Count"
            case (R.id.buttonOne4FindCount) :
               startFragments(NormalGame.newInstance(),"NormalGame", FindCountTest.newInstance(getCountPlanet()));
                break;
            case (R.id.buttonTwo4FindCount) :
                startFragments(TotalGame.newInstance(),"TotalGame", FindCountTest.newInstance(getCountPlanet()));
                break;
            case (R.id.buttonThree4FindCount) :
                startFragments(QuickGame.newInstance(),"QuickGame", FindCountTest.newInstance(getCountPlanet()));
                break;
            case (R.id.buttonFour4FindCount) :
                startFragments(RecordGame.newInstance(),"RecordGame", FindCountTest.newInstance(getCountPlanet()));
                break;

//                                             "Examples 6"
            case (R.id.buttonOne6Examples) :
                CreateExample.setIsPlus(true);
                startFragments(NormalGame.newInstance(),"NormalGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonTwo6Examples) :
                CreateExample.setIsMinus(true);
                startFragments(NormalGame.newInstance(),"NormalGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonThree6Examples) :
                startFragments(NormalGame.newInstance(),"NormalGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonFour6Examples) :
                startFragments(QuickGame.newInstance(),"QuickGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonFive6Examples) :
                startFragments(TotalGame.newInstance(),"TotalGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonSix6Examples) :
                startFragments(RecordGame.newInstance(),"RecordGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            //                                             "Examples 4"
            case (R.id.buttonOne4Examples) :
                startFragments(NormalGame.newInstance(),"NormalGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonTwo4Examples) :
                startFragments(TotalGame.newInstance(),"TotalGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonThree4Examples) :
                startFragments(QuickGame.newInstance(),"QuickGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonFour4Examples) :
                startFragments(RecordGame.newInstance(),"RecordGame", ExamplesTest.newInstance(getCountPlanet()));
                break;

//                                              "MoreLess"
            case (R.id.buttonOne4MoreLess) :
                startFragments(NormalGame.newInstance(),"NormalGame", MoreLessTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonTwo4MoreLess) :
                startFragments(TotalGame.newInstance(),"TotalGame", MoreLessTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonThree4MoreLess) :
                startFragments(QuickGame.newInstance(),"QuickGame", MoreLessTest.newInstance(getCountPlanet()));
                break;

            case (R.id.buttonFour4MoreLess) :
                startFragments(RecordGame.newInstance(),"RecordGame", MoreLessTest.newInstance(getCountPlanet()));
                break;
        }
    }

    private void startFragments(Fragment gameProgress, String tagProgress, Fragment gameType){
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.add(R.id.gameProgressFragmentContainer, gameProgress,tagProgress);
        transaction.add(R.id.gameTypeFragmentContainer, gameType);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void setImageSpaceship (){
        imageSpaceship.setImageResource(spaceship());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(layoutPlanet(),container,false);
        Log.i(TAG,"onCreateView");
        myDbHelper = MyDbHelper.getInstance(getContext());
        imageSpaceship = view.findViewById(R.id.imageSpaceshipContainer);
        setImageSpaceship();
        imageSpaceship.setOnClickListener(this);
        initButtons(view);

        btnArr = new AppCompatImageButton[]{mOneFindCountButton, mTwoFindCountButton, mThreeFindCountButton, mFourFindCountButton,
                mOneExamplesButton, mTwoExamplesButton, mThreeExamplesButton, mFourExamplesButton, mFiveExamplesButton, mSixExamplesButton,
                mOneMoreLessButton, mTwoMoreLessButton, mThreeMoreLessButton, mFourMoreLessButton};

        setIsActiveBtn();

        return view;
    }

    public void initIsActiveBtn(String planet, String [] buttons){
        for (int i = 0; i < btnArr.length; i++) {
            if(btnArr.length == buttons.length) {
                if (btnArr[i] != null) {
                    if (myDbHelper.getIsActiveButton(planet, buttons[i]) == 0) btnArr[i].setEnabled(false);
                    else  btnArr[i].setEnabled(true);
                }
            }
        }
    }



    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG,"onDestroyView");
    }
}
