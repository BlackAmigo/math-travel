package com.app.amigo.mathtravel.gameTypes.moreLess;

import android.os.Bundle;

public class MoreLessTest extends MoreLess {

    private static int planetCount;

    @Override
    protected int count() {
        return planetCount;
    }

    public static MoreLessTest newInstance(int getCountPlanet) {
        planetCount = getCountPlanet;
        Bundle args = new Bundle();
        MoreLessTest fragment = new MoreLessTest();
        fragment.setArguments(args);
        return fragment;
    }
}
