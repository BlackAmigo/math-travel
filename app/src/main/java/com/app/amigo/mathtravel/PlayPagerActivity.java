package com.app.amigo.mathtravel;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import android.widget.LinearLayout;

import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.database.MyDbHelper;
import com.app.amigo.mathtravel.game.OnBackPressedListener;
import com.app.amigo.mathtravel.planets.*;

import java.util.ArrayList;
import java.util.List;

public class PlayPagerActivity extends AppCompatActivity {

    public static final String TAG = "PlayPagerActivity";
    private static PagerAdapter adapter;
    private static ViewPager mViewPager;

    public static TextView mCounterGold;
    public static TextView mCounterFuel;
    private MyDbHelper myDbHelper;

    public static LinearLayout playPagerToolbar;

    public static ViewPager getmViewPager() {
        return mViewPager;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");

        setContentView(R.layout.activity_play_pager);
        playPagerToolbar = findViewById(R.id.toolbar_play_pager);
        playPagerToolbar.setBackground(getResources().getDrawable(R.drawable.toolbar_play_pager_background));
        initViewPager();

        myDbHelper = MyDbHelper.getInstance(this);

        mCounterGold = findViewById(R.id.counter_gold);
        mCounterGold.setText(myDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));
        mCounterFuel = findViewById(R.id.counter_fuel);
        mCounterFuel.setText(myDbHelper.getCurrentAward(MyDatabase.FUEL_COLUMN));
    }

        private void initViewPager(){
            Log.i(TAG,"initViewPager");

            mViewPager = (ViewPager) findViewById(R.id.view_pager_container);

            adapter = new PagerAdapter(getSupportFragmentManager());
            adapter.addFragment(FirstPlanet.getFirstPlanet());
            adapter.addFragment(SecondPlanet.getSecondPlanet());
            adapter.addFragment(ThirdPlanet.getThirdPlanet());
            adapter.addFragment(FourthPlanet.getFourthPlanet());
            adapter.addFragment(FifthPlanet.getFifthPlanet());

            mViewPager.setAdapter(adapter);
        }

   class PagerAdapter extends FragmentStatePagerAdapter{

    private final List<Fragment> mFragments = new ArrayList<>();

    private PagerAdapter(FragmentManager manager) {
        super(manager);
    }

       private void addFragment (Fragment fragment){
        mFragments.add(fragment);
        notifyDataSetChanged();

    }

        @Override
        public int getCount () {
        return mFragments.size();
    }

        @Override
        public Fragment getItem ( int position){
        return mFragments.get(position);
    }
    }

    public static Fragment getCurrentFragment() {
        return adapter.getItem(mViewPager.getCurrentItem());
    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        OnBackPressedListener onBackPressedListener = null;
        for (Fragment fragment: fm.getFragments()) {
            if (fragment instanceof  OnBackPressedListener) {
                onBackPressedListener = (OnBackPressedListener) fragment;
                break;
            }
        }
            if(onBackPressedListener != null){
                onBackPressedListener.onBackPressed();
            }else {
                super.onBackPressed();
            }
        }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"onDestroy");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG,"onResume");
    }

}
