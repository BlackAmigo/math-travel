package com.app.amigo.mathtravel.planets;

import android.support.v4.app.Fragment;
import android.view.View;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.buildFragment.SecondSpaceship;
import com.app.amigo.mathtravel.database.MyDatabase;

public class SecondPlanet extends Planet {

    private static SecondPlanet secondPlanet;

    private SecondSpaceship secondSpaceship = SecondSpaceship.getSecondSpaceship();

    @Override
    int layoutPlanet() {
        return R.layout.fragment_planet_two;
    }

    @Override
    int getCountPlanet() {
        return CountPlanet.getCountSecondPlanet();
    }

    @Override
    void initButtons(View view) {
        initFindCountBtn(view);
        initExamplesBtn(view,true);
        initMoreLessBtn(view);
    }

    @Override
    public void setIsActiveBtn() {
        initIsActiveBtn(MyDatabase.TWO_PLANET, MyDatabase.BUTTONS);
    }

    @Override
    Fragment buildFragment() {
        return SecondSpaceship.getSecondSpaceship();
    }

    @Override
    int spaceship() {
        if(myDbHelper.allParts(secondSpaceship)){
            return R.drawable.rocket_two_with_flame;
        }else  return R.drawable.rocket_two_scheme;
    }

    public static SecondPlanet getSecondPlanet() {
        if(secondPlanet == null){
            secondPlanet = new SecondPlanet();
            return secondPlanet;
        }else {
            return secondPlanet;
        }
    }
}
