package com.app.amigo.mathtravel.game;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.amigo.mathtravel.PlayPagerActivity;
import com.app.amigo.mathtravel.R;

public class RocketBuiltFragment extends Fragment implements View.OnClickListener {

    private final static String TAG = "RocketBuiltFragment";

    private Button continueBtn;
    private TextView txt;
    private ViewPager viewPager;
    private static boolean FivePlanet = false;

    public static RocketBuiltFragment newInstance() {

        Bundle args = new Bundle();
        RocketBuiltFragment fragment = new RocketBuiltFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static boolean isFivePlanet() {
        return FivePlanet;
    }

    public static void setFivePlanet(boolean fivePlanet) {
        RocketBuiltFragment.FivePlanet = fivePlanet;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_rocket_built, container, false);

        continueBtn = view.findViewById(R.id.continueBuiltBtn);
        continueBtn.setOnClickListener(this);
        txt = view.findViewById(R.id.txt_info);

        if(!FivePlanet)txt.setText("Следущая Планета Открыта!");
        else txt.setText("Ракеты Построены!");
        continueBtn.setText("Продолжить");

        viewPager = PlayPagerActivity.getmViewPager();
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.continueBuiltBtn) {
            getFragmentManager().popBackStack();
            getFragmentManager().popBackStack();

            if (viewPager.getCurrentItem() < 4) {
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                    }
                };
                Handler h = new Handler();
                h.postDelayed(runnable, 50);

            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FivePlanet = false;
    }
}
