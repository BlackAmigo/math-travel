package com.app.amigo.mathtravel.planets;

import android.support.v4.app.Fragment;
import android.view.View;

import com.app.amigo.mathtravel.PlayPagerActivity;
import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.buildFragment.FirstSpaceship;
import com.app.amigo.mathtravel.database.MyDatabase;

public class FirstPlanet extends Planet {

    private static FirstPlanet firstPlanet;

    private FirstSpaceship firstSpaceship = FirstSpaceship.getFirstSpaceShip();

    @Override
    int layoutPlanet() {
        return R.layout.fragment_planet_one;
    }

    @Override
    int getCountPlanet() {
        return CountPlanet.getCountFirstPlanet();
    }

    @Override
    void initButtons(View view) {
        initFindCountBtn(view);
        initMoreLessBtn(view);
    }

    @Override
    public void setIsActiveBtn() {

        initIsActiveBtn(MyDatabase.ONE_PLANET, MyDatabase.BUTTONS);
        mOneFindCountButton.setEnabled(true);
        mOneMoreLessButton.setEnabled(true);

    }


    @Override
    Fragment buildFragment() {
        return FirstSpaceship.getFirstSpaceShip();
    }

    @Override
    int spaceship() {
        if(myDbHelper.allParts(firstSpaceship)){
            return R.drawable.rocket_one_with_flame;
        }else  return R.drawable.rocket_one_scheme;
    }

    public static FirstPlanet getFirstPlanet() {
        if(firstPlanet == null){
            firstPlanet = new FirstPlanet();
            return firstPlanet;
        }else {
            return firstPlanet;
        }
    }

}
