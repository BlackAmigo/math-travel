package com.app.amigo.mathtravel.gameProgress;

import android.os.Bundle;
import android.view.View;

public class RecordGame extends GameProgressFragment {

    private static int counterRecordGameFragment;
    private static RecordGame recordGameFragment;

    public static int getCounterRecordGameFragment() {
        return counterRecordGameFragment;
    }

    public static void setCounterRecordGameFragment(int counterRecordGameFragment) {
        RecordGame.counterRecordGameFragment = counterRecordGameFragment;
    }

    public static void setRecordGameFragment(RecordGame recordGameFragment) {
        RecordGame.recordGameFragment = recordGameFragment;
    }

    @Override
    int counterFragment() {
        return counterRecordGameFragment;
    }

    @Override
    long timeOut() {
        return 2000;
    }

    @Override
    void createView(View view) {
        createProgressBar(false,view);
        createTimeProgressBar(true,view);
        createRecordLayout(true,view);
    }

    public static RecordGame newInstance() {
        counterRecordGameFragment++;
        Bundle args = new Bundle();
        recordGameFragment = new RecordGame();
        recordGameFragment.setArguments(args);
        return recordGameFragment;
    }

    public  static RecordGame getFragment (){
        if(recordGameFragment != null)return recordGameFragment;
        else return null;
    }

    public void startMyTotalTask (long delayMillis){
        startMyBarTask(delayMillis);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        recordGameFragment = null;
    }
}
