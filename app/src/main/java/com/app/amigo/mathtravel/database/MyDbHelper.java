package com.app.amigo.mathtravel.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;

import com.app.amigo.mathtravel.buildFragment.BuildFragment;

public class MyDbHelper {
    private static MyDatabase myDatabase;
    private static MyDbHelper myDbHelper;
    private Cursor cursor;
    private SQLiteDatabase db;

//    public MyDbHelper(Context context) {
//        myDatabase = new MyDatabase(context);
//    }

    public static MyDbHelper getInstance(Context context){
        myDatabase = new MyDatabase(context);
        if(myDbHelper == null)return new MyDbHelper();
        else return myDbHelper;
    }
    public String getCurrentAward (String myDatabaseField){
        db = myDatabase.getReadableDatabase();
        cursor = db. rawQuery("select * from income_table where _id = ?", new String[] { "total" });
        String s = "";

        if (cursor.moveToFirst()) {
            // определяем номера столбцов по имени в выборке
            int fieldColIndex = cursor.getColumnIndex(myDatabaseField);
            do {
                // получаем значения по номерам столбцов и пишем все в лог
                s = cursor.getString(fieldColIndex);
                // переход на следующую строку
                // а если следующей нет (текущая - последняя), то false - выходим из цикла
            } while (cursor.moveToNext());
        } else{
            Log.d("log", "0 rows");
        }
        cursor.close();
//        db.close();
        return s;
    }

    public void updateAward(String myDatabaseField, int count){
        // создаем объект для данных
        ContentValues contentValues = new ContentValues();
        // подключаемся к БД
        db = myDatabase.getWritableDatabase();

        cursor = db.query(MyDatabase.INCOME_TABLE, null, null, null, null, null, null);

        contentValues.put(myDatabaseField, count);

        db.update(MyDatabase.INCOME_TABLE, contentValues, "_id = ?", new String[] { MyDatabase.TOTAL_LINE });

//        if(myDatabaseField.equals(MyDatabase.GOLD_COLUMN)) goldCount.setText(getCurrentAward(myDatabaseField));
//        else expCount.setText(getCurrentAward(myDatabaseField));

        cursor.close();
    }

    public int getIsBuyPart (String planetField, String partField){
        db = myDatabase.getReadableDatabase();
        cursor = db. rawQuery("select * from buy_table where _id = ?", new String[] { planetField });
        int i = -1;

        if (cursor.moveToFirst()) {
            int fieldColIndex = cursor.getColumnIndex(partField);
            do {
                i = cursor.getInt(fieldColIndex);
            } while (cursor.moveToNext());
        } else{
            Log.d("log", "getIsBuyPart    is:" + i );
        }
        cursor.close();
        return i;
    }

    public void updateBuyPart(String planetField, String partField, int count){
        // создаем объект для данных
        ContentValues contentValues = new ContentValues();
        // подключаемся к БД
        db = myDatabase.getWritableDatabase();

        cursor = db.query(MyDatabase.BUY_TABLE, null, null, null, null, null, null);

        contentValues.put(partField, count);

        db.update(MyDatabase.BUY_TABLE, contentValues, "_id = ?", new String[] { planetField });

        cursor.close();
    }

    /**
     * @return true, если все части ракеты куплены.
     * */
    public boolean allParts(BuildFragment buildFragment){
        boolean ifAll = false;
        for (int i = 0; i < buildFragment.countParts(); i++) {
            if(getIsBuyPart(buildFragment.planet(), MyDatabase.getParts()[i]) == 1){
                ifAll = true;
            }else {
                ifAll = false;
                break;
            }
        }
        return ifAll;
    }

    public int getIsActiveButton (String planetField, String buttonField){
        db = myDatabase.getReadableDatabase();
        cursor = db. rawQuery("select * from button_table where _id = ?", new String[] { planetField });
        int i = -1;

        if (cursor.moveToFirst()) {
            int fieldColIndex = cursor.getColumnIndex(buttonField);
            do {
                i = cursor.getInt(fieldColIndex);
            } while (cursor.moveToNext());
        } else{
            Log.d("log", "getIsActiveButton    is:" + i );
        }
        cursor.close();
        return i;
    }

    public void updateIsActiveButton(String planetField, String buttonField, int count){
        // создаем объект для данных
        ContentValues contentValues = new ContentValues();
        // подключаемся к БД
        db = myDatabase.getWritableDatabase();

        cursor = db.query(MyDatabase.BUTTON_TABLE, null, null, null, null, null, null);

        contentValues.put(buttonField, count);

        db.update(MyDatabase.BUTTON_TABLE, contentValues, "_id = ?", new String[] { planetField });

        cursor.close();
    }

}
