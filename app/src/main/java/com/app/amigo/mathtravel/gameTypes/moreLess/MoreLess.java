package com.app.amigo.mathtravel.gameTypes.moreLess;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.app.amigo.mathtravel.R;
import com.app.amigo.mathtravel.gameProgress.GameProgressFragment;

public abstract class MoreLess extends CreateMoreLess implements View.OnClickListener {

    private TextView txtA;
    private TextView txtB;
    private TextView txtSign;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.game_more_less,container,false);

            initView(view);
            clickableDelay();

        return view;
    }

    private void initView(View view){
        randomExample();

        txtA = view.findViewById(R.id.txtA);
        txtA.setText(String.valueOf(getA()));
        txtB = view.findViewById(R.id.txtB);
        txtB.setText(String.valueOf(getB()));
        txtSign = view.findViewById(R.id.txtSign);
        txtSign.setText("  ?  ");

        Button btnMore = view.findViewById(R.id.btnLess);
        btnMore.setText(" < ");
        Button btnEquals = view.findViewById(R.id.btnEquals);
        btnEquals.setText("=");
        Button btnLess = view.findViewById(R.id.btnMore);
        btnLess.setText(" > ");

        btnMore.setOnClickListener(this);
        btnEquals.setOnClickListener(this);
        btnLess.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        if (clickable) {
            clickable = false;

        int i = 2;
            switch (v.getId()){
                case (R.id.btnLess) : i = -1;
                    break;
                case (R.id.btnEquals) : i = 0;
                    break;
                case (R.id.btnMore) : i = 1;
                    break;
            }

            if(i == getAnswer()){
                setTextColor(true, txtSign, txtA, txtB);
                if(getCurrentCounterFragment() > 9){
                    GameProgressFragment.l10.setBackgroundResource(R.drawable.progress_bar_style_green);
                    addFragmentIfTrueAnswer();
                    mMyDbHelper.updateIsActiveButton(getStringCurrentPlanet(),getCurrentButton(),1);
                    updateButton();
                }else {replaceFragmentsIfRightAnswer();}

            } else if (i != getAnswer()){
                setTextColor(false, txtSign, txtA, txtB);
                addFragmentIfFalseAnswer();
            }
        }
    }

}
