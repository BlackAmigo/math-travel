package com.app.amigo.mathtravel.gameProgress;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

public class TotalGame extends GameProgressFragment {

    private static int counterTotalGameFragment = 0;
    private static TotalGame totalGameFragment;

    public static int getCounterTotalGameFragment() {
        return counterTotalGameFragment;
    }

    public static void setCounterTotalGameFragment(int counterTotalGameFragment) {
        TotalGame.counterTotalGameFragment = counterTotalGameFragment;
    }

    @Override
    int counterFragment() {
        return counterTotalGameFragment;
    }

    @Override
    long timeOut() {
        return 8000;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTotal = true;
    }

    @Override
    void createView(View view) {
        createProgressBar(true,view);
        createTimeProgressBar(true,view);
        createRecordLayout(false,view);
    }

    public static TotalGame newInstance() {
        Bundle args = new Bundle();
        counterTotalGameFragment++;
        totalGameFragment = new TotalGame();
        totalGameFragment.setArguments(args);
        return totalGameFragment;
    }

    public static TotalGame getFragment (){
        if(totalGameFragment != null)return totalGameFragment;
        else return null;
    }

    public void startMyTotalTask(long delayMillis){
        startMyBarTask(delayMillis);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    //    totalGameFragment = null;
        isTotal = false;
    }
}
