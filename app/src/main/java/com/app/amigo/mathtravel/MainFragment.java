package com.app.amigo.mathtravel;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatImageButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.database.MyDbHelper;
import com.app.amigo.mathtravel.planets.FirstPlanet;

public class MainFragment extends Fragment implements View.OnClickListener{

    public static final String TAG = "MainFragment";

    private TextView mCounterGold;
    private TextView mCounterFuel;
    public Button mSettingButton;
    public AppCompatImageButton mPlayButton;
    public Button mBuildButton;
    public Button mMapButton;
    public RelativeLayout fuelLayout;
    public RelativeLayout goldLayout;
    public FragmentTransaction mTransaction;

    public Fragment mSettingFragment;
    public Fragment mPlayFragment;
    public Fragment mBuildFragment;
    public Fragment mMapFragment;

    private MyDbHelper myDbHelper;


    public static MainFragment newInstance() {
       return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");
        mSettingFragment = new SettingFragment();
        mPlayFragment = new FirstPlanet();
//        mBuildFragment = BuildFragment.getSecondPlanet();
        mMapFragment = new MapFragment();
        myDbHelper = MyDbHelper.getInstance(getContext());
    }

    private void setButtonStyle(AppCompatImageButton button, @DrawableRes int drawableRes, @DrawableRes int drawableResSmall){
        if (button != null) {

            VectorDrawableCompat vcAccept = VectorDrawableCompat.create(getResources(), drawableRes, null);
            VectorDrawableCompat vcAcceptSmall = VectorDrawableCompat.create(getResources(), drawableResSmall, null);

            StateListDrawable stateList = new StateListDrawable();
            stateList.addState(new int[]{android.R.attr.state_focused, -android.R.attr.state_pressed}, vcAccept);
            stateList.addState(new int[]{android.R.attr.state_focused, android.R.attr.state_pressed}, vcAcceptSmall);
            stateList.addState(new int[]{-android.R.attr.state_focused, android.R.attr.state_pressed}, vcAcceptSmall);
            stateList.addState(new int[]{}, vcAccept);

            button.setImageDrawable(stateList);
            button.setPadding(0,0,0,0);
            button.setBackgroundColor(Color.TRANSPARENT);
            button.setScaleType(ImageView.ScaleType.FIT_XY);
        }
    }

    private void initButtons (View view){
        Log.i(TAG,"initButtons");

        fuelLayout = view.findViewById(R.id.fuel_layout);
        goldLayout = view.findViewById(R.id.gold_layout);
        mCounterGold = view.findViewById(R.id.counter_gold);
        mCounterFuel = view.findViewById(R.id.counter_fuel);

        mSettingButton = view.findViewById(R.id.button_setting);
        mPlayButton = view.findViewById(R.id.button_play);
        setButtonStyle(mPlayButton, R.drawable.btn_main_play, R.drawable.btn_main_play_small);
//        mBuildButton = view.findViewById(R.id.button_build);
        mMapButton = view.findViewById(R.id.button_map);

        fuelLayout.setOnClickListener(this);
        goldLayout.setOnClickListener(this);

        mSettingButton.setOnClickListener(this);
        mPlayButton.setOnClickListener(this);
//        mBuildButton.setOnClickListener(this);
        mMapButton.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Log.i(TAG,"onClick");

    switch (v.getId()){
        case R.id.button_setting : replaceFragment(mSettingFragment);
            break;
        case R.id.button_play :
            Intent intent = new Intent(getContext(),PlayPagerActivity.class);
            startActivity(intent);
            break;
        case R.id.button_build : replaceFragment(mBuildFragment);
            break;
        case R.id.button_map : replaceFragment(mMapFragment);
            break;
        }
    }

    private void replaceFragment(Fragment fragment){
        Log.i(TAG,"replaceFragment");

        try{

        mTransaction = getFragmentManager().beginTransaction();
        mTransaction.replace(R.id.fragmentContainer, fragment);
        mTransaction.addToBackStack(null);
        mTransaction.commit();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main,container,false);
        Log.i(TAG,"onCreateView");

        initButtons(view);

        return view;
    }
    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG,"onDestroyView");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i(TAG,"onResume");
        mCounterGold.setText(myDbHelper.getCurrentAward(MyDatabase.GOLD_COLUMN));
        mCounterFuel.setText(myDbHelper.getCurrentAward(MyDatabase.FUEL_COLUMN));
    }
}
