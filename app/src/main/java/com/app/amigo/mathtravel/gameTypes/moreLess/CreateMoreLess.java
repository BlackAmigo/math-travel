package com.app.amigo.mathtravel.gameTypes.moreLess;

import com.app.amigo.mathtravel.game.HelperTypes;

public abstract class CreateMoreLess extends HelperTypes {

    private int a;
    private int b;
    private int answer;

    protected abstract int count();

    protected int getA() {
        return a;
    }

    protected int getB() {
        return b;
    }

    protected int getAnswer(){
        return answer;
    }

    protected void randomExample () {

        a = (int) (Math.random() * count());
        b = (int) (Math.random() * count());
        if ((a - b) > 0) answer = 1;
        else if ((a - b) < 0) answer = -1;
        else answer = 0;

    }
}
