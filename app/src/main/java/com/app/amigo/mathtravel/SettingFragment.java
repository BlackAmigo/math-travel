package com.app.amigo.mathtravel;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.app.amigo.mathtravel.database.MyDatabase;
import com.app.amigo.mathtravel.database.MyDbHelper;

public class SettingFragment extends Fragment implements View.OnClickListener {
    public static final String TAG = "SettingFragment";
    private MyDbHelper mMyDbH;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(TAG,"onCreate");
        mMyDbH = MyDbHelper.getInstance(getContext());
        setHasOptionsMenu(false);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting,container,false);
        Log.i(TAG,"onCreateView");

        Button clearGold = view.findViewById(R.id.clearGold);
        Button clearExp = view.findViewById(R.id.clearFuel);
        Button clearPart = view.findViewById(R.id.clearPart);
        Button clearActiveBtn = view.findViewById(R.id.clearActiveBtn);
        Button addMoney = view.findViewById(R.id.addMoney);
        clearGold.setOnClickListener(this);
        clearExp.setOnClickListener(this);
        clearPart.setOnClickListener(this);
        clearActiveBtn.setOnClickListener(this);
        addMoney.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case (R.id.clearGold) :
                mMyDbH.updateAward(MyDatabase.GOLD_COLUMN,0);
                break;

            case (R.id.clearFuel) :
                mMyDbH.updateAward(MyDatabase.FUEL_COLUMN,0);
                break;

            case (R.id.clearPart) :
                for (int i = 0; i < MyDatabase.getPlanets().length; i++) {
                    for (int j = 0; j < MyDatabase.getParts().length; j++) {
                        mMyDbH.updateBuyPart(MyDatabase.getPlanets()[i],MyDatabase.getParts()[j],0);
                    }
                }
                break;

            case (R.id.clearActiveBtn) :
                for (int i = 0; i < MyDatabase.getPlanets().length; i++) {
                    for (int j = 0; j < MyDatabase.BUTTONS.length; j++) {
                        mMyDbH.updateIsActiveButton(MyDatabase.getPlanets()[i], MyDatabase.BUTTONS[j], 0);
                    }
                }
                break;

            case (R.id.addMoney) :
                mMyDbH.updateAward(MyDatabase.GOLD_COLUMN,1000);
                mMyDbH.updateAward(MyDatabase.FUEL_COLUMN,1000);
                break;
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG,"onDestroyView");
    }
}
