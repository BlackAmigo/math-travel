package com.app.amigo.mathtravel;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

public class SplashFinalActivity extends AppCompatActivity {

    public static final String TAG = "SplashFinalActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_final);

        Runnable r = new Runnable() {
            @Override
            public void run(){

                Intent intent = new Intent(SplashFinalActivity.this, MainActivity.class);
                startActivity(intent);
                finish();

            }
        };
        Handler h = new Handler();
        h.postDelayed(r, 500);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG,"onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG,"onDestroy");
    }
}
